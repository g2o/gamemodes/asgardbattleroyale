
local bossTable = {
    "NEWWORLD\\NEWWORLD.ZEN" : [
        {
            name = "Czarny Troll",
            position = {x = 48456.6, y = 7996.95, z = 39391.9, angle = 122},
            health = 5500,
            damage = 250,
            magicDamage = 250,
            protection = 90,
            magicProtection = 10,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITMW_BERSERKERAXT", 1],
                ["ITRU_BELIARSRAGE", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
            ]            
        },
        {
            name = "Troll",
            position = {x = 65653.4, y = 1552.89, z = -32100, angle = 42},
            health = 5500,
            damage = 250,
            magicDamage = 250,
            protection = 90,
            magicProtection = 10,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITMW_1H_SPECIAL_04", 1],
                ["ITMW_BELIARWEAPON_1H_01", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
            ]   
        },
        {
            name = "Xardas",
            position = {x = 12064.1, y = 998.125, z = -3323, angle = 22},
            health = 3000,
            damage = 150,
            magicDamage = 200,
            protection = 50,
            magicProtection = 80,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITRU_BELIARSRAGE", 1],
                ["ITMW_BELIARWEAPON_1H_01", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
            ]   
        },
        {
            name = "Szaman",
            position = {x = 31567.3, y = 4554.3, z = -40047, angle = 162},
            health = 5500,
            damage = 100,
            magicDamage = 100,
            protection = 30,
            magicProtection = 80,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITMW_1H_SPECIAL_04", 1],
                ["ITMW_BERSERKERAXT", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
            ]   
        }
    ],
    "ADDON\\ADDONWORLD.ZEN" : [
        {
            name = "Vatras",
            position = {x = 4701.8, y = -982.969, z = 1999.77, angle = 242},
            health = 5000,
            damage = 100,
            magicDamage = 100,
            protection = 30,
            magicProtection = 80,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITMW_BERSERKERAXT", 1],
                ["ITRU_BELIARSRAGE", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
                ["ITMW_BELIARWEAPON_1H_01", 1],
            ]   
        },
        {
            name = "Kruk",
            position = {x = 19890.9, y = -2287.27, z = 18700.3, angle = 242},
            health = 5000,
            damage = 100,
            magicDamage = 100,
            protection = 30,
            magicProtection = 80,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITMW_BERSERKERAXT", 1],
                ["ITRU_BELIARSRAGE", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
                ["ITMW_BELIARWEAPON_1H_01", 1],
            ]   
        },
        {
            name = "Greg",
            position = {x = -35291.2, y = -1873.36, z = 18894.8, angle = 242},
            health = 5000,
            damage = 100,
            magicDamage = 100,
            protection = 30,
            magicProtection = 80,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITAR_RAVEN_ADDON",1],
                ["ITMW_BERSERKERAXT", 1],
                ["ITRU_BELIARSRAGE", 1],
                ["ITAR_XARDAS", 1],
                ["ITRU_WATERFIST", 1],
            ]   
        }
    ],
    "OLDWORLD\\OLDWORLD.ZEN" : [
        {
            name = "Beliar",
            position = {x = 11629.6, y = 12560.6, z = -2585, angle = 242},
            health = 10000,
            damage = 200,
            magicDamage = 200,
            protection = 30,
            magicProtection = 30,
            drop = [
                ["ITAR_OREBARON_ADDON",1],
                ["ITMW_BERSERKERAXT", 1],
                ["ITMW_BELIARWEAPON_1H_01", 1],
            ]   
        }
    ]
}

function Boss_CreateRandom() {
    foreach(boss in bossTable[getServerWorld()])
    {
        local bot = createBoss(boss.name, boss.position.x, boss.position.y, boss.position.z, boss.position.angle);
        bot.health = boss.health;
        bot.maxHealth = boss.health;
        bot.magicDamage = boss.magicDamage;
        bot.damage = boss.damage;
        bot.protection = boss.protection;
        bot.magicProtection = boss.magicProtection;
        bot.drop = boss.drop;        
    }
}