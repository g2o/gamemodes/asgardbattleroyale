function isPlayer(pid) {
    return pid >= 0 && pid < getMaxSlots();
}

function isNpc(pid) {
    return !isPlayer(pid);
}
