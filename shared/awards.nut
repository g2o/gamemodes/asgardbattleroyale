
function Award_onStartGame()
{
    local awards = getBotsJarkendar();

    if(Game.activeWorld == Worlds.Khorinis)
        awards = getBotsKhorinis();
    else if(Game.activeWorld == Worlds.Dolina)
        awards = getBotsDolina();

    for(local i = 0; i < 20; i ++)
    {
        local randomAwardPoistion = rand() % (awards.len() -1);
        Structure(StructureType.Bonus,awards[randomAwardPoistion][0], awards[randomAwardPoistion][1], awards[randomAwardPoistion][2], awards[randomAwardPoistion][3]);
    }
}