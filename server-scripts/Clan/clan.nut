local All = {};

class Clan
{
    id = -1;
    name = "";

    color = null;
    members = null;
    requests = null;

    constructor()
    {
        id = Clan.getFreeId();

        color = {r = 0, g = 0, b = 0}
        members = [];
        requests = [];

        All[id] <- this;
    }

    function addMember(pid, rank) {
        local clanMember = ClanMember();

        clanMember.name = getPlayerName(pid);
        clanMember.serial = getPlayerSerial(pid);
        clanMember.clanId = id;

        clanMember.rankId = rank;
        clanMember.online = true;
        clanMember.onlineId = pid;

        members.append(clanMember);
    }

    function send(pid = -1) {
        local packet = Packet(Packets.Clan);
        packet.writeUInt8(ClanPackets.Create);
        packet.writeInt16(id);
        packet.writeString(name);
        packet.writeInt16(color.r);
        packet.writeInt16(color.g);
        packet.writeInt16(color.b);

        if(pid == -1)
            packet.sendToAll(RELIABLE_ORDERED);
        else
            packet.send(pid, RELIABLE_ORDERED);
    }

    function setColor(r,g,b) {
        color.r = r;
        color.g = g;
        color.b = b;

        local packet = Packet(Packets.Clan);
        packet.writeUInt8(ClanPackets.Color);
        packet.writeInt16(id);
        packet.writeInt16(r);
        packet.writeInt16(g);
        packet.writeInt16(b);
        packet.sendToAll(RELIABLE_ORDERED);
    }

    function readPlayerPermission(pid)
    {
        if(getPlayerClan(pid) != id)
            return 0;

        foreach(member in members)
            if(member.onlineId == pid)
                return member.rankId + 1;
    }

    function calculateClanPoints()
    {
        local points = 0;
        foreach(member in members)
        {
            if(member.online)
                points = points + getPlayer(member.onlineId).CountPoints();
            else{
                points = points + countPointsBySerial(member.serial);
            }
        }
        return points;
    }

    function isLeader(playerId)
    {
        foreach(member in members)
        {
            if(member.online && member.onlineId == playerId)
            {
                if(member.rankId == ClanRank.Leader)
                    return true;

                break;
            }
        }
        return false;
    }

    function tryToAddRequestedUser(playerId, playerName)
    {
        if(requests.find(playerId) == null)
            return;

        if(getPlayerName(playerId) != playerName)
            return;

        if(getPlayerClan(playerId) != -1)
            return;

        addMember(playerId, ClanRank.Member);
        setPlayerClan(playerId, id);

        noteToMembers("Do��czy� do klanu "+playerName);

        foreach(_clan in All)
            if(_clan.requests.find(playerId) != null)
                _clan.requests.remove(_clan.requests.find(playerId));

        Clan.save();
    }

    function noteToMembers(text)
    {
        foreach(member in members)
        {
            if(member.online)
            {
                smallNote(member.onlineId, text)
            }
        }
    }

    function addRequestForJoining(playerId)
    {
        if(requests.find(playerId) != null)
            return;

        requests.append(playerId);
        noteToMembers(getPlayerName(playerId) + " chce do��czy� do klanu.");
    }

    function tryToRemoveMember(playerName)
    {
        foreach(memberIndex, member in members)
        {
            if(member.name == playerName)
            {
                if(member.rankId == ClanRank.Leader)
                {
                    Clan.removeClan(id);
                    return;
                }

                if(member.online)
                    setPlayerClan(member.onlineId, -1);

                noteToMembers("Usuni�to z klanu "+playerName);
                members.remove(memberIndex);
                Clan.save();
                return;
            }
        }
    }

    static function onDisconnect(pid) {
        foreach(_clan in All)
            if(_clan.requests.find(pid) != null)
                _clan.requests.remove(_clan.requests.find(pid));

        foreach(_clan in All) {
            foreach(member in _clan.members) {
                if(member.onlineId == pid) {
                    member.online = false;
                    member.onlineId = -1;
                    return;
                }
            }
        }
    }

    static function getAll() {
        return All;
    }

    static function get(id)
    {
        if(id in All)
            return All[id];

        return null;
    }

    static function onJoin(pid) {
        foreach(_clan in All)
        {
            _clan.send(pid);

            foreach(member in _clan.members)
                member.checkIsOnline();
        }
    }

    static function removeClan(clanId)
    {
        foreach(_index, _clan in All)
        {
            if(_clan.id == clanId)
            {
                foreach(_member in _clan.members)
                    if(_member.online)
                        setPlayerClan(_member.onlineId, -1);

                smallNoteToAll("Usuni�to klan "+_clan.name);
                All.rawdelete(_index);
                Clan.save();
                break;
            }
        }

        local packet = Packet(Packets.Clan);
        packet.writeUInt8(ClanPackets.Destroy);
        packet.writeInt16(clanId);
        packet.sendToAll(RELIABLE_ORDERED);
    }

    static function load()
    {
        local myfile = file("clans.txt", "r");
        local newClan = true;
        local stage = 0;
        local clanObj = null;
        local args = 0;
        do{
            args = myfile.read("l");
            if(args != null)
            {
                switch(stage)
                {
                    case 0:
                        local arg = sscanf("d", args);
                        clanObj = Clan();
                        clanObj.id = arg[0];
                        stage = 1;
                    break;
                    case 1:
                        local arg = sscanf("s", args);
                        clanObj.name = arg[0];
                        stage = 2;
                    break;
                    case 2:
                        local arg = sscanf("ddd", args);
                        clanObj.color = { r = arg[0], g = arg[1], b = arg[2] }
                        stage = 3;
                    break;
                    case 3:
                        local arg = sscanf("dss", args)
                        if(!arg)
                        {
                            stage = 0;
                            continue;
                        }

                        local clanMember = ClanMember();

                        clanMember.name = arg[2];
                        clanMember.serial = arg[1];
                        clanMember.clanId = clanObj.id;

                        clanMember.rankId = arg[0];

                        clanObj.members.append(clanMember);
                    break;
                }
            }
            else{
                break;
            }
        } while (args != null)

        myfile.close();
    }

    static function save()
    {
        local myfile = file("clans.txt", "w");
        foreach(_clan in All)
        {
            myfile.write(_clan.id + "\n");
            myfile.write(_clan.name + "\n");
            myfile.write(_clan.color.r + " " + _clan.color.g + " " + _clan.color.b + "\n");
            foreach(mem in _clan.members)
                myfile.write(mem.rankId+ " " + mem.serial + " " + mem.name + "\n");

            myfile.write("===========\n");
        }
        myfile.close();
    }

    static function getFreeId() {
        local _id = 0;

        foreach(_clan in All)
            _id = _clan.id + 1;

        return _id;
    }

    static function getAll()
    {
        return All;
    }
}