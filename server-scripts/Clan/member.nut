
class ClanMember
{
    clanId = -1;

    rankId = -1;

    online = false;
    onlineId = -1;
    
    serial = "";
    name = "";

    function checkIsOnline() {
        foreach(pid, player in getAllPlayers()) {
            if(player != null) {
                if(getPlayerSerial(pid) == serial) {
                    online = true;
                    onlineId = pid;
                    setPlayerClan(pid, clanId);
                    local color = Clan.get(clanId).color;
                    setPlayerColor(pid, color.r, color.g, color.b);
                    return;
                }
            }
        }

        onlineId = -1;
        online = false;
    }
}