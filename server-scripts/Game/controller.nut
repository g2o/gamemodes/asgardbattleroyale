
Game.Controller <- {
    "onEnter" : function (pid) {
        local obj = getPlayer(pid);

        local packet = Packet(Packets.Game);
        packet.writeUInt8(GamePackets.Enter);
        packet.writeUInt8(Game.status);
        packet.writeBool(obj.isAdmin);

        if(Game.status == GameStatus.Game)
        {
            packet.writeFloat(Game.randomCircleSpawn.x);
            packet.writeFloat(Game.randomCircleSpawn.y);
            packet.writeFloat(Game.randomCircleSpawn.z);
            packet.writeString(Game.activeWorld);

            if(getPlayerWorld(pid) != getServerWorld())
                setPlayerWorld(pid, getServerWorld());
        }

        packet.send(pid, RELIABLE_ORDERED);

        switch(Game.status)
        {
            case GameStatus.Game:
                getPlayer(pid).inGame = true;
            break;
            case GameStatus.Lobby:
                local packet = Packet(Packets.Player);
                packet.writeUInt8(PlayerPackets.PlayVideo);
                packet.send(pid, RELIABLE_ORDERED);
            break;
        }
    },
    "onExit" : function (pid) {

    },
    "tryStartGame" : function (pid, worldId) {
        if(Game.timer == -1) {
            Game.timer = 0;
        }

        if(Game.status != GameStatus.Lobby)
            return false;

        if(Game.timer != 0)
            return false;

        local worldName = "";
        switch(worldId)
        {
            case 0:
                worldName = "Jarkendar";
            break;
            case 1:
                worldName = "Stara dolina";
            break;
            case 2:
                worldName = "Khorinis";
            break;
        }

        Game.votes[worldId] = Game.votes[worldId] + 1;

        addLobbyQuerie("Gracz "+getPlayerName(pid)+" jest gotowy do gry. �wiat "+worldName);
        getPlayer(pid).readyToPlay = true;
        getPlayer(pid).choosenWorld = worldId;
        Game.checkGameCanBeStarted();
    },
    "startGame" : function () {
        Game.timer = 60 * 45;
        Game.status = GameStatus.Game;
        Game.Controller.circleMath();
        
        local worldId = 0;
        local voteWinning = Game.votes[0];

        foreach(worldIndex, voteValue in Game.votes)
        {
            if(voteWinning < voteValue) {
                worldId = worldIndex;
                voteWinning = voteValue;
            }
        }

        switch(worldId)
        {
            case 0:
                if(Game.activeWorld != Worlds.Jarkendar)
                {
                    setServerWorld(Worlds.Jarkendar);
                    Game.activeWorld = Worlds.Jarkendar;
                }
            break;
            case 1:
                if(Game.activeWorld != Worlds.Dolina)
                {
                    setServerWorld(Worlds.Dolina);
                    Game.activeWorld = Worlds.Dolina;
                }
            break;
            case 2:
                if(Game.activeWorld != Worlds.Khorinis)
                {
                    setServerWorld(Worlds.Khorinis);
                    Game.activeWorld = Worlds.Khorinis;
                }
            break;
        }

        foreach(player in getPlayers())
        {
            if(isPlayerConnected(player.id))
            {   
                if(getPlayerWorld(player.id) != getServerWorld())
                    setPlayerWorld(player.id, getServerWorld());

                Game.goOnRandomSpawn(player.id);
                spawnPlayer(player.id);
                player.onStartGame();
            }
        }

        Award_onStartGame();
        Bot_onStartGame();
        Structure.onStartGame();
        setTime(7, 10);

        Game.randomCircleSpawn = Game.circleRands[Game.activeWorld][rand() % Game.circleRands[Game.activeWorld].len()];
        Game.circle.setPosition(Game.randomCircleSpawn.x, Game.randomCircleSpawn.y, Game.randomCircleSpawn.z);

        local packet = Packet(Packets.Game);
        packet.writeUInt8(GamePackets.StartGame);
        packet.writeFloat(Game.randomCircleSpawn.x);
        packet.writeFloat(Game.randomCircleSpawn.y);
        packet.writeFloat(Game.randomCircleSpawn.z);
        packet.writeString(Game.activeWorld);
        packet.sendToPlayers(RELIABLE_ORDERED);
    },
    "endGame": function() {
        Game.timer = -1;
        Game.status = GameStatus.Lobby;
        Game.votes = [0,0,0];

        clearLobbyQueries();
        setOrcPlayer(-1); 

        foreach(player in getPlayers())
        {
            if(isPlayerConnected(player.id))
            {
                unspawnPlayer(player.id);
                player.onEndGame();
                setPlayerScale(player.id, 1.0, 1.0, 1.0);
            }
        }

        Bot_onEndGame();
        Drop.onEndGame();
        ItemInGroundBucket.onEndGame();
        Structure.onEndGame();
        Memory.Flush();

        Game.lobbyChatBlocked = false;

        local packet = Packet(Packets.Game);
        packet.writeUInt8(GamePackets.EndGame);
        packet.sendToPlayers(RELIABLE_ORDERED);
    },
    "circleMath" : function () {
        if(Game.timer <= 1800)
        {
            local radiusGameTime = Game.timer - 100;
            if(radiusGameTime < 0)
                radiusGameTime = 0;
                
            local newRadius = abs(radiusGameTime * 60);
            if(newRadius < 10)
                newRadius = 1;

            local verticesCount = newRadius/100;

            if(verticesCount < 50)
                verticesCount = 50;
                
            if(Game.timer <= 360 && Game.timer > 200)
                Game.circle.polyY = {min = Game.randomCircleSpawn.y - 1500, max = Game.randomCircleSpawn.y + 1500}
            else if(Game.timer <= 200)
                Game.circle.polyY = {min = Game.randomCircleSpawn.y - 400, max = Game.randomCircleSpawn.y + 400}
            else
                Game.circle.polyY = null;

            Game.circle.setVerticesCount(verticesCount);
            Game.circle.setRadius(newRadius);
        }
    },
    "onTimer" : function () {
        if(Game.timer == -1)
        {
            Game.timer = 0;
            return;
        }
        if(Game.timer == 0)
            return;

        Game.timer--;
        switch(Game.status)
        {
            case GameStatus.Lobby:
                addLobbyQuerie("[#FF0000]Do startu pozosta�o "+Game.timer+"s.");
            break;
            case GameStatus.Game:
                Game.Controller.circleMath();
                Game.checkPlayerStateInGame();
                OrcTimer();

                local packet = Packet(Packets.Game);
                packet.writeUInt8(GamePackets.Timer);
                packet.writeInt16(Game.timer);
                packet.writeInt16(Game.totalInGame);
                packet.sendToPlayers(RELIABLE_ORDERED);
            break;
        }

        if(Game.timer == 0)
        {
            switch(Game.status)
            {
                case GameStatus.Lobby:
                    Game.Controller.startGame();
                break;
                case GameStatus.Game:
                    Game.Controller.endGame();
                break;
            }
        }
    }
}


