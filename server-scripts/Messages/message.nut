

function sendPwMessage(from, to, message) {
	if(Player[to].pwMode == true)
	{
		sendMessageToPlayer(to, 0, 190, 160, getPlayerName(from)+">>"+message);
	}else{
    	local packet = Packet(Packets.Message);
		packet.writeUInt8(MesssagePackets.Receive);
		packet.writeInt16(from);
		packet.writeString(message);
    	packet.send(to, RELIABLE_ORDERED);    
	}
}

addEventHandler("onPlayerCommand", function(pid, cmd, params){
	if(cmd == "pw" || cmd == "pm")
	{
		local args = sscanf("ds", params);
		if (!args)
		{
			sendMessageToPlayer(pid, 255, 0, 0, "ACP: Wpisz /pw id wiadomosc");
			return;
		}

		sendPwMessage(pid, args[0], args[1]);
		sendMessageToPlayer(pid, 0, 160, 190, getPlayerName(args[0])+"<<"+args[1]);
	}
	if(cmd == "pwMode")
	{
		local args = sscanf("ds", params);
		if (!args)
		{
			sendMessageToPlayer(pid, 255, 0, 0, "ACP: Wpisz /pwMode <1 - gui> <0 - tekst>");
			return;
		}	

		if(args[0] == 1)
			Player[pid].pwMode = false;

		if(args[0] == 0)
			Player[pid].pwMode = true;
	}
});