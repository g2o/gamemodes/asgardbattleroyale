addPacketListener(Packets.Ranking, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case RankingPackets.SendRanking:
            sendRankingToPlayer(pid, packet.readInt16());
        break;
        case RankingPackets.SendOnline:
            sendOnlineToPlayer(pid, packet.readInt16());
        break;
    }
})