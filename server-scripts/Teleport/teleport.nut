local All = [];

class Teleport
{
    id = -1;

    location = null;
    teleport = null;

    npcElement = null;

    constructor() {
        location = {};
        teleport = {};

        All.append(this);

        id = All.len() - 1;
    }

    function checkWereIn(pid) {
        local pos = getPlayerPosition(pid);
        local dist = getDistance3d(pos.x, pos.y, pos.z, location.x, location.y, location.z);
        if(dist < 300)
            return true;
        else
            return false;
    }

    function use(pid) {
        if(!checkWereIn(pid))
            return;

        addEffect(pid, "spellFX_Fear");
        setPlayerPosition(pid, teleport.x, teleport.y, teleport.z);
    }
}


function getTeleport(id) {
    return All[id];
}

function addTeleport(locx,locy,locz,destx,desty,destz) {
    local model = Teleport();
    model.location = { x = locx, y = locy, z = locz}
    model.teleport = { x = destx, y = desty, z = destz}
}
