
local All = [];

class Drop
{
    id = -1;

    item = null;
    owner = -1;

    time = 60;

    constructor(instance, amount, _owner, x,y,z)
    {
        id = All.len();

        item = ItemsGround.spawn(Items.id(instance), amount, x,y,z,getServerWorld())

        owner = _owner;

        add3dDraw("DRAW"+id, getPlayerName(owner), x, y + 100, z, 0, 200, 0);

        All.append(this);
    }

    function remove(withItem = false) {
        remove3dDraw("DRAW"+id)

        try {
            if(withItem)
                ItemsGround.destroy(item.id);
        }catch(exc) {

        }
    }

    static function onEndGame()
    {
        foreach(_item in All)
            _item.remove(true);

        All.clear();
    }
}

setTimer(function () {
    foreach(item in All)
    {
        if(item.time == 0)
            continue;

        if(item.time > 0)
            item.time = item.time - 1;

        if(item.time <= 0)
        {
            remove3dDraw("DRAW"+item.id)
            item.time = 0;
        }
    }
}, 1000, 0);

function getAllDropedItems() {
    return All;
}