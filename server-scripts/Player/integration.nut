
addPacketListener(Packets.Player, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case PlayerPackets.UseSkill:
            playerUseSkill(pid, packet.readUInt8())
        break;
        case PlayerPackets.ResetSkill:
            resetPlayerSkillTimeouts(pid);
        break;
        case PlayerPackets.Mount:
            addMountForPlayer(pid);
        break;
    }
})