
local orcPlayer = -1;

function setOrcPlayer(orcId) 
{
    orcPlayer = orcId;  

    if(orcPlayer != -1)
        sendMessageToAll(255, 80, 0, format("Nowy �owca w grze: %s", getPlayerName(orcId)));  

    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.Orc);
    packet.writeInt16(orcPlayer);
    packet.sendToAll(RELIABLE_ORDERED);
}

function getOrcPlayer() {
    return orcPlayer;
}

function OrcTimer() {
    if(orcPlayer != -1)
        return;

    if(Game.timer < 1800)
        setOrcPlayer(getRandomPlayer());
}

function checkOnDisconnectOrcPlayer(pid) {
    if(pid == orcPlayer)
        setOrcPlayer(getRandomPlayer());
}

addEventHandler("onPlayerJoin", function (pid) {
    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.Orc);
    packet.writeInt16(orcPlayer);
    packet.send(pid, RELIABLE_ORDERED);
})

addEventHandler("onPlayerDead", function(playerid, killerid)
{
    if(playerid == orcPlayer && killerid != -1)
    {
        setPlayerHealth(killerid, 1000);
        setOrcPlayer(killerid);
    }
    else if(playerid == orcPlayer && killerid == -1)
    {
        setOrcPlayer(getRandomPlayer());
    }
});