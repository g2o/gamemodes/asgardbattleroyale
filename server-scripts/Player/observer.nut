

function enterObserver(pid) {
    setPlayerInvisible(pid, true);
    setPlayerScale(pid, 0.1, 0.1, 0.1);

    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.Observer);
    packet.send(pid, RELIABLE_ORDERED);
}