local All = {};

class Draw3d
{
    text = "";
    position = null;
    color = null;

    constructor(text, x, y, z, r, g, b)
    {
        this.text = text;
        this.position = {x = x, y = y, z = z}
        this.color = { r = r, g = g, b = b}
    }
}

function Draw3d_onPlayerJoin(pid) {
    foreach(id, draw in All)
    {
        local packet = Packet(Packets.Draws)
        packet.writeUInt8(DrawPackets.Create);
        packet.writeString(id);
        packet.writeString(draw.text);
        packet.writeFloat(draw.position.x);
        packet.writeFloat(draw.position.y);
        packet.writeFloat(draw.position.z);
        packet.writeInt16(draw.color.r);
        packet.writeInt16(draw.color.g);
        packet.writeInt16(draw.color.b);
        packet.send(pid, RELIABLE_ORDERED);  
    }
}

function add3dDraw(id, text, x, y, z, r = 255, g = 255, b = 255) {
    All[id] <- Draw3d(text, x, y, z, r, g, b);

    local packet = Packet(Packets.Draws)
    packet.writeUInt8(DrawPackets.Create);
    packet.writeString(id);
    packet.writeString(text);
    packet.writeFloat(x);
    packet.writeFloat(y);
    packet.writeFloat(z);
    packet.writeInt16(r);
    packet.writeInt16(g);
    packet.writeInt16(b);
    packet.sendToPlayers(RELIABLE_ORDERED);
}

function remove3dDraw(id) {
    All.rawdelete(id);

    local packet = Packet(Packets.Draws)
    packet.writeUInt8(DrawPackets.Destroy);
    packet.writeString(id);
    packet.sendToPlayers(RELIABLE_ORDERED);
}