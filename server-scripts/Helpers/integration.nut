
addPacketListener(Packets.Helpers, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case HelperPackets.AddPlayer:
            addPlayerToSlot(packet.readInt16(), packet.readString())
        break;
        case HelperPackets.RemovePlayer:
            removePlayerFromSlot(packet.readInt16())
        break;
        case HelperPackets.AddLobby:
            if(Game.lobbyChatBlocked == true)
                return;
                
            addLobbyQuerie(packet.readString())
        break;
        case HelperPackets.Print:
            local text = packet.readString();
            print(text)
        break;
        case HelperPackets.Kick:
            kick(pid, packet.readString())
        break;
    }
})