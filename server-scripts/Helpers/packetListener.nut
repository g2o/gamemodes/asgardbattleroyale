
local listeners = [];

function addPacketListener(id, _func) {
    listeners.append({ id = id, func = _func});
}

function packetListener(pid, packet) {
    local id = packet.readUInt8();

    foreach(listen in listeners)
        if(listen.id == id)
            listen.func(pid, packet);
}