
local ticks = {};

class createBoss extends Bot
{
    drop = null;
    enemies = null;
    basePosition = null;
    lastAttackerId = -1;

    constructor(name, x, y, z, angle)
    {
        base.constructor(name);

        this.position = {x = x, y = y, z = z}
        this.basePosition = {x = x, y = y, z = z}
        this.angle = angle

        this.schemeId = BotScheme.Boss;
        this.ai = BotAI.Search;

        this.enemy = -1;
        this.timer = 0;
        this.enemies = [];

        ticks[id] <- getTickCount() + 1000;

        instance = "";
        drop = [];

        this.onPositionUpdate();
    }

    function writeAdditionalInformations(packet) {
        packet.writeUInt8(ai);
    }

    function onTimer() {
        if(exist == false) {
            ticks[id] = getTickCount() + 1000;
            return;
        }

        if(ai == BotAI.Search)
            searchNearbyEnemy();
        else if(ai == BotAI.Attack)
            attackEnemy();
        else if(ai == BotAI.Dead)
            goesDead();
    }

    function addEnemy(pid)
    {
        if (enemies.find(pid))
            return pid;
    
        if(isPlayerConnected(pid)) {
            foreach(bot in getPlayerHelpers(pid))
                bot.addEnemy(id);
        }

        enemies.append(pid);
        return enemies[enemies.len()-1];
    }

    function removeEnemy(pid)
    {
        foreach(index, enemyId in enemies)
            if(enemyId == pid)
                enemies.remove(index);
    }

    function goesDead()
    {
        removeBot(id);
    }

    function attackEnemy() {
        if(getEnemy() == -1)
        {
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 100;
            return;
        }

        local distance = getDistanceBetweenPositions(getEnemyPosition(), position);
        if(distance > 1600)
        {
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 1000;
            return;
        }

        turnIntoEnemy();
        
        if(distance > 500)
        {
            local randAttack = rand() % 4;
            switch(randAttack)
            {
                case 2:
                    hitTarget(BotBossAttack.RangeAdditional);
                break;
                default:
                    hitTarget(BotBossAttack.Range);
                break;
            }
            setBotPosition(id, basePosition.x, basePosition.y, basePosition.z);
            ticks[id] = getTickCount() + 1500;
            return;
        } else {     
            local randAttack = rand() % 4;
            switch(randAttack)
            {
                case 2:
                    hitTarget(BotBossAttack.MeleeAdditional);
                break;
                default:
                    hitTarget(BotBossAttack.Melee);
                break;
            }
            ticks[id] = getTickCount() + 1000;
            return;
        }        
    }

    function searchNearbyEnemy() {
        local lastDistance = 1600;
        local positionObject = null;

        foreach(pid,player in getAllPlayers())
            if(player != null)
                if(getDistanceBetweenPositions(getPlayerPosition(pid), position) <= 1600)
                    addEnemy(pid);

        foreach(index, enemyId in enemies)
        {
            if(isNpc(enemyId))
            {
                local bObj = getBot(enemyId);
                if(bObj == null)
                {
                    enemies.remove(index);
                    continue;
                }
                if(bObj.health <= 0)
                {
                    enemies.remove(index);
                    continue;
                }

                positionObject = bObj.position;
                local dist = getDistanceBetweenPositions(positionObject, position);
                if(dist < lastDistance)
                {
                    lastDistance = dist;
                    setEnemy(enemyId);
                }
                else if(dist > 1000)
                {
                    enemies.remove(index);
                    continue;
                }
            }
            else
            {
                if(!isPlayerConnected(enemyId))
                {
                    enemies.remove(index);
                    continue;
                }
                if(!isPlayerSpawned(enemyId))
                {
                    enemies.remove(index);
                    continue;
                }
                if(getPlayerInvisible(enemyId))
                {
                    enemies.remove(index);
                    continue;
                }
                positionObject = getPlayerPosition(enemyId);
                local dist = getDistanceBetweenPositions(positionObject, position);
                if(dist < lastDistance)
                {
                    lastDistance = dist;
                    setEnemy(enemyId);
                }
                else if(dist > 1600)
                {
                    enemies.remove(index);
                    continue;
                }
            }
        }

        ticks[id] = getTickCount() + 1000;
    }


    function setEnemy(enemyId) {
        enemy = enemyId;
        ai = BotAI.Attack;
    }

    function getEnemy() {
        if(enemy == -1)
            return enemy;

        if(enemy == 0)
        {
            if(!isPlayerConnected(enemy))
                enemy = -1;
            else if(!isPlayerSpawned(enemy))
                enemy = -1;
            else if(getPlayerHealth(enemy) <= 0)
                enemy = -1;
            else if(getPlayerInvisible(enemy))
                enemy = -1;
        }else {
            local bObj = getBot(enemy);
            if(bObj == null)
                enemy = -1;
            else if(bObj.health <= 0)
                enemy = -1;
        }

        return enemy;
    }

    function getEnemyPosition()
    {
        if(enemy == 0)
            return getPlayerPosition(enemy);
        else
            return getBotPosition(enemy);
    }

    function turnIntoEnemy()
    {
        local pos = getEnemyPosition();
        local _angle = getVectorAngle(position.x,position.z,pos.x,pos.z);
        local angleDiff = abs(_angle - angle);

        if(angleDiff > 10)
            setBotAngle(id, _angle);
    }

    function hitTarget(typeAttackId) {
        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.BossAttack);
        packet.writeInt16(id);
        packet.writeInt16(enemy);
        packet.writeUInt8(typeAttackId);
        packet.sendToPlayersInTable(RELIABLE_ORDERED, visiblePlayers);

        if(isNpc(enemy))
            getBot(enemy).getAttackedByNpc(this);
    }

    function attackPlayer(pid) {
        local dmg = calculateDamageToPlayerByNPC(this, pid, DAMAGE_BEAST);

        if(dmg <= 3)
            dmg = 3;

        Bot_addHelperBotsEnemy(pid, id);

        local hp = getPlayerHealth(pid) - dmg;
        if(hp < 0) {
            hp = 0;
            setBotAnimation(id, "STOP");
            enemy = -1;
            ai = BotAI.Search;
        }

        setPlayerHealth(pid, hp);
    }

    function playerKillBot(pid) {
        ticks[id] = getTickCount() + 5000;
        ai = BotAI.Dead;

        for(local i = 0; i <= DROP_AMOUNT_FROM_BOT; i = i + 1)
        {
            local chance = rand() % drop.len();
            local dropItem = drop[chance];

            Drop(dropItem[0], dropItem[1], pid, position.x + rand() % 100,position.y + 50, position.z + rand() % 100);
        }

        local chanceForArrows = rand() % 100;

        if(chanceForArrows < 20)
            Drop("ITRW_ARROW", 20, pid, position.x - rand() % 100,position.y + 50, position.z - rand() % 100);
        else if(chanceForArrows < 40)
            Drop("ITRW_BOLT", 20, pid, position.x - rand() % 100,position.y + 50, position.z - rand() % 100);

        setPlayerBuildingPoints(pid, getPlayerBuildingPoints(pid) + 50);
    }

    function getAttackedByPlayer(pid) {
        local dmg = calculateDamageForNPC(pid, this);

        if(getDistanceBetweenPositions(getPlayerPosition(pid), position) >= 1800)
            return;

        if(getEnemy() == -1)
        {
            local newEnemyId = addEnemy(pid);
            setEnemy(newEnemyId);
            ai = BotAI.Attack;
            ticks[id] = getTickCount() + 100;
        }

        if(lastAttackerId == pid && getEnemy() != pid)
        {
            local newEnemyId = addEnemy(pid);
            setEnemy(newEnemyId);
        }

        Bot_addHelperBotsEnemy(pid, id);

        if(dmg <= 3)
            dmg = 3;

        health = health - dmg;

        if(health <= 0) {
            health = 0;
            playerKillBot(pid);
            removeBot(id);
            return;
        }

        lastAttackerId = pid;
        setBotHealth(id, health);
    }

    function getAttackedByNpc(bot)
    {
        local dmg = calculateDamageToNPCByNPC(bot, this, DAMAGE_EDGE);

        if(getEnemy() == -1)
        {
            local newEnemyId = addEnemy(bot.id);
            setEnemy(newEnemyId);
            ai = BotAI.Attack;
            ticks[id] = getTickCount() + 100;
        }

        if(lastAttackerId == bot.id && getEnemy() != bot.id)
        {
            local newEnemyId = addEnemy(bot.id);
            setEnemy(newEnemyId);
        }

        lastAttackerId = bot.id;

        if(dmg <= 3)
            dmg = 3;

        health = health - dmg;

        if(health <= 0) {
            health = 0;
            removeBot(id);

            if(bot instanceof createHelper)
            {
                local playerHelperId = bot.helperUserId;
                for(local i = 0; i <= DROP_AMOUNT_FROM_BOT; i = i + 1)
                {
                    local chance = rand() % drop.len();
                    local dropItem = drop[chance];

                    Drop(dropItem[0], dropItem[1], playerHelperId, position.x + rand() % 100,position.y - 50, position.z + rand() % 100);
                }

                local chanceForArrows = rand() % 100;

                if(chanceForArrows < 20)
                    Drop("ITRW_ARROW", 20, playerHelperId, position.x - rand() % 100,position.y - 50, position.z - rand() % 100);
                else if(chanceForArrows < 40)
                    Drop("ITRW_BOLT", 20, playerHelperId, position.x - rand() % 100,position.y - 50, position.z - rand() % 100);
            }
            return;
        }

        setBotHealth(id, health);
    }

    function beforeRemove()
    {
        ticks.rawdelete(id);
    }

    ai = -1;
    enemy = -1;

    timer = -1;
}

setTimer(function () {
    local current = getTickCount();
    foreach(botId, time in ticks)
    {
        if(time <= current) {
            getBot(botId).onTimer();
        }
    }
}, 100, 0);

function clearBossBotTicks() {
    ticks.clear();
}