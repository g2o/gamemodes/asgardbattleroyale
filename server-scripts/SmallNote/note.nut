

function smallDmgNote(dmgid, value, pos)
{
    local packet = Packet(Packets.SmallNotes);
    packet.writeUInt8(SmallNotesPackets.Add);
    packet.writeUInt8(NoteType.Damage);
    packet.writeString(value);
    packet.writeInt16(dmgid);
    packet.sendInDistanceRange(pos, 1500, RELIABLE_ORDERED);
    packet = null;
}

function smallNoteToAll(value)
{
    local packet = Packet(Packets.SmallNotes);
    packet.writeUInt8(SmallNotesPackets.Add);
    packet.writeUInt8(NoteType.Information);
    packet.writeString(value);
    packet.sendToAll(RELIABLE_ORDERED);
    packet = null;
}

function smallNote(pid, value)
{
    local packet = Packet(Packets.SmallNotes);
    packet.writeUInt8(SmallNotesPackets.Add);
    packet.writeUInt8(NoteType.Information);
    packet.writeString(value);
    packet.send(pid, RELIABLE_ORDERED);
    packet = null;
}

function smallKillNote(value)
{
    local packet = Packet(Packets.SmallNotes);
    packet.writeUInt8(SmallNotesPackets.Add);
    packet.writeUInt8(NoteType.Kill);
    packet.writeString(value);
    packet.sendToAll(RELIABLE_ORDERED);
    packet = null;
}