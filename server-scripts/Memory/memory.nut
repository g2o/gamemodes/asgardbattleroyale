
Memory <- {};

Memory.Admins <- [];
Memory.Users <- [];
Memory.Items <- {};

function Memory::AdminRecordRead(serial) {
    foreach(admin in Memory.Admins)
        if(admin.serial == serial)
            return true;

    return false;
}

function Memory::Flush()
{
    Memory.Users.clear();
    Memory.Items.clear();
}

function Memory::UserRecord(serial, name, x, y, z, hp, hearts, buildingPoints, timeout0, timeout1, timeout2, timeout3, timeout4, points, bonus1, bonus2, bonus3, bonus4) {
    Memory.Users.append({serial = serial, name = name, x = x, y = y, z = z, hp = hp, hearts = hearts, buildingPoints = buildingPoints, timeout0 = timeout0, timeout1 = timeout1, timeout2 = timeout2, timeout3 = timeout3, timeout4 = timeout4, points = points, bonus1 = bonus1, bonus2 = bonus2, bonus3 = bonus3, bonus4 = bonus4});
}

function Memory::UserRecordRead(serial) {
    local lastrecord = null;

    foreach(index, user in Memory.Users)
        if(user.serial == serial)
            lastrecord = {record = user, index = index};

    if(lastrecord != null) {
        Memory.Users.remove(lastrecord.index);
    }

    return lastrecord == null ? null : lastrecord.record;
}

function Memory::ItemsRecord(serial, id, amount) {
    if(!(serial in Memory.Items))
        Memory.Items[serial] <- [];

    Memory.Items[serial].append({itemId = id, amount = amount});
}

function Memory::ItemsRecordRead(serial) {
    local loadeditems = [];

    if(!(serial in Memory.Items))
        return [];

    foreach(index, item in Memory.Items[serial])
    {
        loadeditems.append({ itemId = item.itemId, amount = item.amount })
    }

    Memory.Items.rawdelete(serial);

    return loadeditems;
}