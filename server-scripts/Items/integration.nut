
class ItemIntegration
{
    _Items = {};

    function addItem(instance, item)
    {
        local id = Items.id(instance);
        ItemIntegration._Items[id] <- Item(instance,item);
        return ItemIntegration._Items[id];
    }

    function getItem(id)
    {
        if(!ItemIntegration._Items.rawin(id))
            return null;

        return ItemIntegration._Items[id];
    }

    function packetEqUse(pid)
    {
        local packet = Packet(Packets.Items);
        packet.writeUInt8(ItemPackets.EqUse);
        packet.send(pid,RELIABLE_ORDERED);
    }

    function packetEquipItem(pid, id)
    {
        local packet = Packet(Packets.Items);
        packet.writeUInt8(ItemPackets.Equip);
        packet.writeInt16(id);
        packet.send(pid, RELIABLE_ORDERED);
    }

    function packetUnEquipItem(pid, id)
    {
        local packet = Packet(Packets.Items);
        packet.writeUInt8(ItemPackets.UnEquip);
        packet.writeInt16(id);
        packet.send(pid, RELIABLE_ORDERED);
    }

    function onPacket(pid, packet)
    {
        local id = packet.readUInt8();
        if(id != Packets.Items)
            return;

        id = packet.readUInt8();
        switch(id)
        {
            case ItemPackets.Equip:
                local idItem = packet.readInt16();

                equipItem(pid, idItem, false);
            break;
            case ItemPackets.UnEquip:
                local idItem = packet.readInt16();

                unEquipItem(pid, idItem, false);
            break;
            case ItemPackets.UseItem:
                local idItem = packet.readInt16();
                local amountItem = packet.readInt16();

                usePlayerItem(pid, idItem, amountItem);
            break;
            case ItemPackets.EqUse:
                local idItem = packet.readInt16();

                eqUsePlayerItem(pid, idItem);
            break;
        }
    }
}

addEventHandler("onPacket", ItemIntegration.onPacket);

function addItem(instance, item)
{
    instance = instance.toupper();
    return ItemIntegration.addItem(instance, item);
}

addEventHandler("onInit", function()
{
    local file = file("items.xml", "w")

    file.write("<items>\n")

    foreach(instance, item in RegisteredItems)
        file.write("\t<item><instance>" + instance + "</instance></item>\n")

    file.write("</items>")
})