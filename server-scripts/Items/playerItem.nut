_giveItem <- giveItem;
_removeItem <- removeItem;

_equipItem <- equipItem;
_unequipItem <- unequipItem;

class _PlayerItem
{
    pid = -1;

    loaded = false;

    constructor(_pid)
    {
        pid = _pid;

        items = {};
        equipped = [];
    }

    function sendIntoBucket()
    {
        local pos = getPlayerPosition(pid);
        local idItem = -1;
        local amountItem = -1;
        local itemsCount = items.len();

        if(itemsCount > 2)
        {
            local rand = rand() % itemsCount, i = 0;
            foreach(item, amount in items)
            {
                if(i == rand)
                {
                    idItem = item;
                    amountItem = amount;
                    break;
                }
                i = i + 1;
            }
        }

        if(idItem != -1)
        {
            items.rawdelete(idItem);
            ItemInGroundBucket(idItem, amountItem, pos.x+rand()%200, pos.y, pos.z+rand()%100);
            idItem = -1;
        }
    }

    function clear()
    {
        foreach(itemId, amount in items)
            Memory.ItemsRecord(getPlayerSerial(pid), itemId, amount);

        loaded = false;

        items.clear();
        equipped.clear();
    }

    function respawn() {
        equipped.clear();

        foreach(id, amount in items)
            _giveItem(pid, id, amount);

        equipBestItems();
    }

    function equipBestItems() {

        local bestArmor = {value = 0, id = 0};
        local bestMelee = {value = 0, id = 0};
        local bestRanged = {value = 0, id = 0};

        foreach(id, amount in items)
        {
            local itemObj = ItemIntegration.getItem(id);
            switch(itemObj.flag)
            {
                case ITEM_KAT_ARMOR:
                    if(bestArmor.value < itemObj.protection) {
                        bestArmor.value = itemObj.protection;
                        bestArmor.id = id;
                    }
                break;
                case ITEM_KAT_MELEE:
                    if(bestMelee.value < itemObj.damage) {
                        bestMelee.value = itemObj.damage;
                        bestMelee.id = id;
                    }
                break;
                case ITEM_KAT_RANGED:
                    if(bestRanged.value < itemObj.damage) {
                        bestRanged.value = itemObj.damage;
                        bestRanged.id = id;
                    }
                break;
                case ITEM_KAT_RUNE:
                    _equipItem(pid, id);
                break;
            }
        }

        if(bestArmor.id != 0)
            _equipItem(pid, bestArmor.id)
        if(bestMelee.id != 0)
            _equipItem(pid, bestMelee.id)
        if(bestRanged.id != 0)
            _equipItem(pid, bestRanged.id)        
    }

    items = null;
    equipped = null;
}

PlayerItem <- [];

for(local i = 0; i<= getMaxSlots(); i++)
    PlayerItem.append(_PlayerItem(i));

getItems <- @(id) PlayerItem[id];
getAllItems <- @() PlayerItem;