

AnimationInterface <- {
    Buttons = [],

    Tree = {
        "Taniec": {
            "Taniec 1" : "T_DANCE_09",
            "Taniec 2" : "T_DANCE_08",
            "Taniec 3" : "T_DANCE_07",
            "Taniec 4" : "T_DANCE_06",
            "Taniec 5" : "T_DANCE_05",
            "Taniec 6" : "T_DANCE_04",
            "Taniec 7" : "T_DANCE_03",
            "Wariacki taniec" : "S_FIRE_VICTIM",
        },
        "Mimika": {
            "Mrugni�cie" : ["R_EYESBLINK"],
            "Z�y" : ["S_ANGRY"],
            "Oczy zamkni�te" : ["S_EYESCLOSED"],
            "Przera�ony" : ["S_FRIGHTENED"],
            "Normalny" : ["S_NEUTRAL"],
            "Przyjazny" : ["S_FRIENDLY"],
        },
        "Czynno�ci" : {
            "Wspinaczka" : "S_HANG",
            "Pal" : "PT_JOINT_RANDOM_1al",
            "Modlitwa" : "S_PRAY",
            "Modlitwa +" : "T_IDOL_S0_2_S1",
            "St�j" : "T_STAND_2_HGUARD",
            "Czytaj" : "T_MAP_STAND_2_S0",
            "Naprawiaj" : "S_REPAIR_S1",
            "Myj si�" : "T_WASH_2_STAND",
            "Usi�d�" : "T_STAND_2",
        },
        "Magia" : {
            "Magia 1": "T_PRACTICEMAGIC",
            "Magia 2": "T_PRACTICEMAGIC2",
            "Magia 3": "T_PRACTICEMAGIC3",
            "Magia 4": "T_PRACTICEMAGIC4",
        },
        "R�ne" : {
            "Tak" : "T_YES",
            "Trening" : "T_1HSFREE",
            "Tak" : "T_FOODHUGE_RANDOM_1",
            "Ogl�daj walk�" : "T_WATCHFIGHT_YEAH",
            "Przegl�d miecza" : "T_1HSINSPECT",
            "Alchemia" : "S_LAB_S1",
            "Nie wiadomo!" : "T_DONTKNOW",
        }
    }

    Show = function () {
        BaseGUI.show();
        Player.GUI = PlayerGUIEnum.Animation;
        AnimationInterface.ShowTreeFor(AnimationInterface.Tree);
    }

    Hide = function () {
        BaseGUI.hide();
        Player.GUI = false;

        foreach(butt in AnimationInterface.Buttons)
            butt.button.setVisible(false);

        AnimationInterface.Buttons.clear();
    }

    ShowTreeFor = function(table)
    {
        local degreePerButton = 360/table.len();
        local showedButtons = 0;

        foreach(name, value in table)
        {
            local positionButton = {x = 3596, y = 3596};
            local rotation = degreePerButton * showedButtons;

            positionButton.x = positionButton.x + (sin(rotation * 3.14 / 180.0) * 1200);
            positionButton.y = positionButton.y + (cos(rotation * 3.14 / 180.0) * 1200);

            local button = GUI.Button(positionButton.x, positionButton.y, anx(200), any(70), "BUTTON_ENDLESS", name);
            button.setVisible(true);

            AnimationInterface.Buttons.append({button = button, option = value});	
            showedButtons = showedButtons + 1;
        }
    },

    ClickOption = function(optionButton)
    {
        foreach(option in AnimationInterface.Buttons)
        {
            if(option.button == optionButton)
            {
                if(typeof option.option == "table")
                {
                    foreach(butt in AnimationInterface.Buttons)
                        butt.button.setVisible(false);

                    AnimationInterface.Buttons.clear();
                    AnimationInterface.ShowTreeFor(option.option);
                }else if(typeof option.option == "array")
                {
                    startFaceAni(heroId, option.option[0]);
                    AnimationInterface.Hide();
                }else if(typeof option.option == "string")
                {
                    playAni(heroId, option.option);
                    AnimationInterface.Hide();
                }
                break;
            }
        }
    }
}

bindKey(KEY_ESCAPE, AnimationInterface.Hide, PlayerGUIEnum.Animation)

addEventHandler("onMouseClick", function(btn) {
    if(Player.GUI != false)
        return;

    if (chatInputIsOpen())
		return

	if (isConsoleOpen())
		return

    if(getPlayerWeaponMode(heroId) != WEAPONMODE_NONE)
        return;

    if(btn == MOUSE_RMB)
        AnimationInterface.Show();
})


addEventHandler("GUI.onClick", function(self)
{
    if(Player.GUI != PlayerGUIEnum.Animation)
        return;
    
    AnimationInterface.ClickOption(self);
})