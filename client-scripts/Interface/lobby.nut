
LobbyGUI <- {};

LobbyGUI.Background <- Texture(0,0,8200,8200, "BACKGROUND_ENDLESS.TGA");

LobbyGUI.Background.alpha = 100;

LobbyGUI.LogoGlow <- GUI.Texture(anx(Resolution.x/2 - 256), any(100), anx(512), any(192), "LOGO_ENDLESS.TGA");

LobbyGUI.Menu <- GUI.Window(anx(50), any(Resolution.y/2 - 450), anx(350), any(300), "MAIN_ENDLESS.TGA");
LobbyGUI.StartGame <- GUI.Button(anx(50),any(40),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Gotowy do gry", LobbyGUI.Menu);
LobbyGUI.ExitGame <- GUI.Button(anx(50),any(140),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Wyjd� z Gry", LobbyGUI.Menu);

LobbyGUI.StartGame.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");
LobbyGUI.ExitGame.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");

LobbyGUI.ListPlayers <- GUI.Window(anx(Resolution.x - 500), any(Resolution.y/2 - 450), anx(450), any(700), "MAIN_ENDLESS.TGA");
LobbyGUI.Scroll <- GUI.ScrollBar(anx(380), any(50), anx(30), any(600), "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", Orientation.Vertical, LobbyGUI.ListPlayers);

LobbyGUI.ChatList <- GUI.Window(anx(50), any(Resolution.y - 400), anx(1200), any(400), "");
LobbyGUI.InputChat <- GUI.Input(anx(0),any(310),anx(900),any(60), "INPUT_ENDLESS.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 30, LobbyGUI.ChatList);
LobbyGUI.Submit <- GUI.Button(anx(950),any(300),anx(200),any(80), "BUTTON_ENDLESS.TGA", "Wy�lij", LobbyGUI.ChatList);

LobbyGUI.Submit.draw.setFont("FONT_OLD_10_WHITE_HI.TGA");

local aboutUsMultiDraw = GUI.Draw(anx(Resolution.x/2 - 300), any(Resolution.y/2 - 300),
@"To jest serwer multiplayer do Gothic 2 Online.
No i na chuj wi�cej m�wi�.
Quarchodron zrobi� na po�egnanie.")

local aboutGameMultiDraw = GUI.Draw(anx(Resolution.x/2 - 300), any(Resolution.y/2 - 300),
@"L�dujesz na mapie. Wybierzesz pozycje nacisk�jac czach� na mapie.
Potem zapierdalaj graczy moby i zbieraj drop i skrzynkki i wog�le kurwa.
Po godzinie jak w jebanym fortnite bedzie taki kurwa ma�y okrag �e sie zesrasz.")

LobbyGUI.WorldChooseWindow <- GUI.Window(anx(Resolution.x/2 - 180), any(Resolution.y/2 - 150), anx(350), any(350), "MAIN_ENDLESS.TGA");
LobbyGUI.Jarkendar <- GUI.Button(anx(50),any(40),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Jarkendar", LobbyGUI.WorldChooseWindow);
LobbyGUI.OldWorld <- GUI.Button(anx(50),any(140),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Dolina", LobbyGUI.WorldChooseWindow);
LobbyGUI.NewWorld <- GUI.Button(anx(50),any(240),anx(250),any(80), "BUTTON_ENDLESS.TGA", "Khorinis", LobbyGUI.WorldChooseWindow);
LobbyGUI.ChoosenWorld <- -1;

LobbyGUI.AllPlayers <- [];
LobbyGUI.ChatTable <- [];

local backgroundMusic = Sound("BACKGROUND_ENDLESS.WAV");

function LobbyGUI::show() {
    LobbyGUI.Background.visible = true;
    LobbyGUI.LogoGlow.setVisible(true);
    LobbyGUI.Menu.setVisible(true);
    LobbyGUI.WorldChooseWindow.setVisible(false);
	Camera.setPosition(-1714.4,1105.58,-1415.8);
	Camera.setRotation(12,64,0);
    
    backgroundMusic.play();

	local world = getWorld()

	switch (world)
	{
		case "OLDWORLD\\OLDWORLD.ZEN":
                Camera.setPosition(0,1105.58,0);
                Camera.setRotation(12,64,0);
			break
	}


    LobbyGUI.ListPlayers.setVisible(true);
    LobbyGUI.ChatList.setVisible(true);
    LobbyGUI.LoadPlayers();
    BaseGUI.show();

    LobbyGUI.ChoosenWorld = -1;

    Player.GUI = PlayerGUIEnum.Lobby;
}

function LobbyGUI::hide() {
    LobbyGUI.Background.visible = false;
    LobbyGUI.LogoGlow.setVisible(false);
    LobbyGUI.Menu.setVisible(false);
    LobbyGUI.WorldChooseWindow.setVisible(false);

    aboutUsMultiDraw.setVisible(false);
    aboutGameMultiDraw.setVisible(false);

    backgroundMusic.stop()

    if(Player.isAdmin == false)
        LobbyGUI.StartGame.setVisible(false);

    LobbyGUI.ListPlayers.setVisible(false);
    LobbyGUI.ChatList.setVisible(false);

    foreach(player in LobbyGUI.AllPlayers)
    {
        player.setVisible(false);
        player.destroy();
    }

    LobbyGUI.AllPlayers.clear();

    foreach(chatEntrie in LobbyGUI.ChatTable) {
        chatEntrie.setVisible(false);
        chatEntrie.destroy();
    }

    LobbyGUI.ChatTable.clear();

    BaseGUI.hide();

    Player.GUI = false;
}

function LobbyGUI::showWorldChoose() {
    LobbyGUI.WorldChooseWindow.setVisible(true);
}

function LobbyGUI::hideWorldChoose() {
    LobbyGUI.WorldChooseWindow.setVisible(false);
}

function LobbyGUI::LoadPlayers() {
    foreach(player in LobbyGUI.AllPlayers)
    {
        player.setVisible(false);
        player.destroy();
    }

    local scrollIndex = LobbyGUI.Scroll.getValue();

    LobbyGUI.AllPlayers.clear();

    local playersList = 0;
    foreach(index, player in getAllPlayers())
    {
        if(index >= scrollIndex && index < scrollIndex + 7)
        {
            local currentSlotY = playersList * any(80) + any(50);

            if(player == null)
                continue;

            playersList++;
            LobbyGUI.AllPlayers.append(GUI.Button(anx(Resolution.x - 500) + anx(40), any(Resolution.y/2 - 450) + currentSlotY + any(5), anx(0), any(0), "FRAME_HERO_ENDLESS.TGA", ""));
            LobbyGUI.AllPlayers.append(GUI.Button(anx(Resolution.x - 500) + anx(40), any(Resolution.y/2 - 450) + currentSlotY + any(5), anx(330), any(70), "BUTTON_ENDLESS.TGA", player));

            LobbyGUI.AllPlayers[LobbyGUI.AllPlayers.len() - 1].draw.setFont("FONT_OLD_10_WHITE_HI.TGA");
        }
    }

    foreach(butt in LobbyGUI.AllPlayers)
        butt.setVisible(true);
}

function LobbyGUI::LoadChat() {
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

    foreach(chatEntrie in LobbyGUI.ChatTable) {
        chatEntrie.setVisible(false);
        chatEntrie.destroy();
    }

    LobbyGUI.ChatTable.clear();

    foreach(index, listEntrie in getListLobbyChat())
        if(type(listEntrie) == "string")
            LobbyGUI.ChatTable.append(GUI.Draw(anx(50),any(270 - index * 30),listEntrie, LobbyGUI.ChatList));

    foreach(listEntrie in LobbyGUI.ChatTable)
        listEntrie.setVisible(true);
}

addEventHandler("GUI.onClick", function(self)
{
	switch (self)
	{
		case LobbyGUI.ExitGame:
			exitGame();
        break;
        case LobbyGUI.Submit:
            if(LobbyGUI.InputChat.getText().len() > 2) {
                sendLobbyQuerie("[#af9b57]"+getPlayerName(heroId)+": [#FFFFFF]"+LobbyGUI.InputChat.getText())
                LobbyGUI.InputChat.setText("");
            }
        break;
        case LobbyGUI.StartGame:
            LobbyGUI.showWorldChoose();
        break;
        case LobbyGUI.OldWorld:
            Game.Controller.tryStartGame(1);
            LobbyGUI.StartGame.setVisible(false);
            LobbyGUI.ChoosenWorld = 1;
            LobbyGUI.hideWorldChoose();
        break;
        case LobbyGUI.NewWorld:
            Game.Controller.tryStartGame(2);
            LobbyGUI.StartGame.setVisible(false);
            LobbyGUI.ChoosenWorld = 2;
            LobbyGUI.hideWorldChoose();
        break;
        case LobbyGUI.Jarkendar:
            Game.Controller.tryStartGame(0);
            LobbyGUI.StartGame.setVisible(false);
            LobbyGUI.ChoosenWorld = 0;
            LobbyGUI.hideWorldChoose();
        break;
	}
})

addEventHandler("GUI.onMouseIn", function(self)
{
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

	switch (self)
	{
		case LobbyGUI.ExitGame:
			self.setFile("BUTTON_HOVER_ENDLESS.TGA");
        break;
	}
})

addEventHandler("GUI.onMouseOut", function(self)
{
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

	switch (self)
	{
		case LobbyGUI.ExitGame:
			self.setFile("BUTTON_ENDLESS.TGA");
        break;
	}
})

addEventHandler("onKey", function (key) {
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

    if(key == KEY_RETURN) {
        if(LobbyGUI.InputChat.getText().len() > 2) {
            sendLobbyQuerie("[#af9b57]"+getPlayerName(heroId)+": [#FFFFFF]"+LobbyGUI.InputChat.getText())
            LobbyGUI.InputChat.setText("");
        }
    }
})

addEventHandler("GUI.onChange", function (self) {
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

    if( self == LobbyGUI.Scroll )
        LobbyGUI.LoadPlayers()
})

addEventHandler("onPlayerGetSlot", function (self) {
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

    LobbyGUI.LoadPlayers()
})

addEventHandler("onPlayerLostSlot", function (self) {
    if(Player.GUI != PlayerGUIEnum.Lobby)
        return;

    LobbyGUI.LoadPlayers()
})
