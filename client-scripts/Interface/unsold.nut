
local block = false;

addEventHandler("onKey", function (key) {
    if(key != KEY_F12)
        return;

    if(block == true)
        return;
    
    if(Player.GUI == PlayerGUIEnum.Lobby)
        return;

    if(Player.GUI == PlayerGUIEnum.Observer)
        return;

    if(Player.GUI != false)
    {
        HideActiveGUI();
    }

    local pos = getPlayerPosition(heroId);
    
    setFreeze(false);
    setPlayerPosition(heroId, pos.x, pos.y + 120, pos.z);
    stopAni(heroId);

    block = true;
    setTimer(function() {
        block = false;
    }, 1000 * 30, 1);
})