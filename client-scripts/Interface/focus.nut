local focusPlayer = -1;

addEventHandler("onFocus", function(new, old) {
    if(new == -1 && focusPlayer != -1)
    {
        focusPlayer = -1;
        return;
    }

    if(new == -1)
        return;

    if(isNpc(new))
        return;

    focusPlayer = new;
})

function getFocusedPlayer()
{
    return focusPlayer;
}
