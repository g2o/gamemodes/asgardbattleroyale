
local timer = 0, check = -1, posCheck = false;

function onGetDamageMounting() {
    if(timer != 0)
        MountInterface.stopMounting();
}

MountInterface <- {};
MountInterface.mountingBot <- -1;

local drawStand = Draw(3500, 3000, "St�j w miejscu przez 5 sekund.")

drawStand.font = "FONT_OLD_10_WHITE_HI.TGA";

function MountInterface::startMounting() {
    if (chatInputIsOpen())
		return
		
	if (isConsoleOpen())
		return

    if(Game.timer < 1800) {
        SmallNote(NoteType.Information, "Nie mo�esz u�y� mounta po 30 minucie.")
        return;
    }

    if(getPlayerInstance(heroId) == "PC_HERO")
        timer = 5;
    else {
        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.Mount);
        packet.send(RELIABLE_ORDERED);
        return;
    }

    check = getTickCount() + 1000;
    drawStand.visible = true;
    posCheck = getPlayerPosition(heroId);
}

function MountInterface::stopMounting() {
    timer = 0;
    check = -1;
    drawStand.visible = false;
}

addEventHandler("onRender", function () {
    if(check > getTickCount())
        return;

    if(timer == 0)
        return;

    timer = timer - 1;

    local pos = getPlayerPosition(heroId);
    if(getDistance2d(pos.x, pos.z, posCheck.x, posCheck.z) > 50)
    {
        MountInterface.stopMounting();
        return;
    }

    if(timer <= 0)
    {
        MountInterface.stopMounting();

        PlayerItem.equpiedItems.clear();

        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.Mount);
        packet.send(RELIABLE_ORDERED);
    }

    check = getTickCount() + 1000;    
})

bindKey(KEY_N, MountInterface.startMounting);