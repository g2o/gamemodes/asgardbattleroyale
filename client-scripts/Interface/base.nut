BaseGUI <- {};

Resolution <- getResolution();

function BaseGUI::show(freezeCharacter = true) {
    Camera.setFreeze(true);

    setHudMode(HUD_ALL, HUD_MODE_HIDDEN);

    for(local i = 0; i<200; i++)
      disableKey(i, true);

    disableKey(1, true);
    setFreeze(freezeCharacter)
    setCursorVisible(true)
}

function BaseGUI::hide() {
    disableKey(KEY_1, false);
    disableKey(KEY_2, false);
    disableKey(KEY_3, false);
    disableKey(KEY_4, false);
    disableKey(KEY_5, false);
    disableKey(KEY_6, false);
    disableKey(KEY_7, false);
    disableKey(KEY_8, false);
    disableKey(KEY_9, false);

    Camera.setFreeze(false);
    setFreeze(false)
    setCursorVisible(false)
}

function HideActiveGUI()
{
    switch(Player.GUI)
    {
      case PlayerGUIEnum.Mounting:
          MountInterface.stopMounting();
      break;
      case PlayerGUIEnum.Menu:
          MenuGUI.hide();
      break;
      case PlayerGUIEnum.Ranking:
          RankingGUI.hide();
      break;
      case PlayerGUIEnum.Statistics:
          PlayerGUI.hide()
      break;
      case PlayerGUIEnum.Detector:
          Detector.hide()
      break;
      case PlayerGUIEnum.HelpMenu:
          HelpMenu.hide()
      break;
      case PlayerGUIEnum.Building:
          BuildingGUI.hide();
      break;
      case PlayerGUIEnum.ClanList:
          ClanGUI.hide()
      break;
      case PlayerGUIEnum.ClanInfo:
          ClanGUI.hide();
      break;
      case PlayerGUIEnum.Map:
          Map.hide();
      break;
      case PlayerGUIEnum.Observer:
          Observer.hide();
      break;
    }
}
