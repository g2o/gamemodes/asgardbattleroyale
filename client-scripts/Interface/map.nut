Map <-
{
	texture = Texture(0, 0, 8192, 8192, "")
	world = ""

	coordinates =
	{
		x = -1,
		y = -1,
		width = -1,
		height = -1
	}

	playerMarker = array(getMaxSlots(), null)
    zoneLines = [],
	xRay = Draw(0,0,"X")
}

/*

	Player Marker

*/

class Map.PlayerMarker extends Draw
{
	pid = -1

	constructor(pid)
	{
		base.constructor(0, 0, "")
		this.pid = pid
	}

	function update(x, y)
	{
		setPosition(x, y)

		text = "+ "+getPlayerName(pid)
		font = "FONT_OLD_10_WHITE_HI.TGA";

		if (!visible)
			visible = true
	}
}

/*

	Map

*/

function Map::init()
{
	for (local pid = 0; pid < getMaxSlots(); ++pid)
		playerMarker[pid] = Map.PlayerMarker(pid);
}

function Map::setLevelCoords(world, x, y, width, height)
{
	this.world = world

	coordinates.x = x
	coordinates.y = y
	coordinates.width = width
	coordinates.height = height
}

function Map::changeLevel()
{
	local world = getWorld()

	switch (world)
	{
		case "NEWWORLD\\NEWWORLD.ZEN":
			texture.file = "Map_NewWorld.tga"
			setLevelCoords(world, -28000, 50500, 95500, -42500)
			break

		case "OLDWORLD\\OLDWORLD.ZEN":
			texture.file = "Map_OldWorld.tga"
			setLevelCoords(world, -78500, 47500, 54000, -53000)
			break

		case "ADDON\\ADDONWORLD.ZEN":
			texture.file = "Map_AddonWorld.tga"
			setLevelCoords(world, -47783, 36300, 43949, -32300)
			break
	}
}

function Map::isPlayerAt(pid)
{
	if (!isPlayerCreated(pid))
		return false

	if (getWorld(pid) != world)
		return false

	if(getPlayerClan(pid) != getPlayerClan(heroId))
		return false;

	if(getPlayerClan(pid) == -1 && pid != heroId)
		return false;

	return true
}

function Map::toggleMarkers(value)
{
	for (local pid = 0; pid < getMaxSlots(); ++pid)
	{
		if (!isPlayerAt(pid))
			continue

		playerMarker[pid].visible = value
	}
}

function Map::updatePlayerMarkers()
{
	if (!texture.visible)
		return

	for (local pid = 0; pid < getMaxSlots(); ++pid)
	{
		if (!isPlayerAt(pid))
			continue

		local playerPosition = getPlayerPosition(pid)

		playerPosition.x -= coordinates.x
		playerPosition.z -= coordinates.y

		local maxX = coordinates.width - coordinates.x
		local maxY = coordinates.height - coordinates.y

		playerPosition.x = (playerPosition.x / maxX.tofloat()) * 8192
		playerPosition.z = (playerPosition.z / maxY.tofloat()) * 8192

		playerMarker[pid].update(playerPosition.x, playerPosition.z)
	}

	foreach(zoneLine in zoneLines) {
		zoneLine.visible = true;
		zoneLine.top();
	}
}

function Map::showZoneLines() {
    zoneLines.clear();

    foreach(index, value in Game.circle.polyX)
    {
        local nowX = Game.circle.polyX[index];
        local nowZ = Game.circle.polyZ[index];

        nowX -= coordinates.x
		nowZ -= coordinates.y

		local maxX = coordinates.width - coordinates.x
		local maxY = coordinates.height - coordinates.y

		nowX = (nowX / maxX.tofloat()) * 8192
		nowZ = (nowZ / maxY.tofloat()) * 8192

        zoneLines.append(Draw(nowX, nowZ, "-"));
        zoneLines[zoneLines.len()-1].setColor(200,0,0);
        zoneLines[zoneLines.len()-1].visible = true;
        zoneLines[zoneLines.len()-1].top();
    }
}

function Map::showXRay(position) 
{
	local nowX = position[0];
	local nowZ = position[2];

	nowX -= coordinates.x
	nowZ -= coordinates.y

	local maxX = coordinates.width - coordinates.x
	local maxY = coordinates.height - coordinates.y

	nowX = (nowX / maxX.tofloat()) * 8192
	nowZ = (nowZ / maxY.tofloat()) * 8192

	xRay.visible = true;
	xRay.setPosition(nowX, nowZ);
	xRay.setColor(0, 190, 150);
}

function Map::show()
{
    BaseGUI.show(false);
    DesktopGameGUI.hideOut()
    Player.GUI = PlayerGUIEnum.Map;

	Map.changeLevel()

	Map.toggleMarkers(true)
	Map.texture.visible = true

	if(Game.timer <= 1800)
		Map.showZoneLines();

	if(TimerTeleport.IsActive())
		Map.showXRay(TimerTeleport.GetPosition());
}

function Map::hide()
{
    BaseGUI.hide();
    DesktopGameGUI.showOut();
    Player.GUI = false;
	
	Map.xRay.visible = false;
    Map.zoneLines.clear();

	Map.toggleMarkers(false)
	Map.texture.visible = false
}

function Map::toggle()
{
	!texture.visible ? show() : hide()
}


//	Init

Map.init()

/*

	Events

*/

addEventHandler("onRender", function()
{
	Map.updatePlayerMarkers()
})

addEventHandler("onPlayerDestroy",function()
{
	Map.playerMarker[pid].visible = false
})

bindKey(KEY_M, Map.show);
bindKey(KEY_M, Map.hide, PlayerGUIEnum.Map);