LandingGUI <- {};

local timer = 0;
local check = 0;

local mapTexture = Texture(0,0,anx(Resolution.x),any(Resolution.y), "Map_AddonWorld.tga");
local selectorPosition = Texture(anx(Resolution.x/2-24),any(Resolution.y/2-28),anx(48),any(56), "ICON_SKULL_ENDLESS.TGA");

local levelCords = {x = -47783, y = 36300, width = 43949, height = -32300};

function setLandingLevelCoords(x,y,width,height) {
    levelCords.x = x;
    levelCords.y = y;
    levelCords.width = width;
    levelCords.height = height;
}

local texture = Texture(anx(Resolution.x/2 - 400), any(Resolution.y - 360), anx(800), any(300), "MAIN_ENDLESS.TGA");
local draw1 = Draw(anx(Resolution.x/2 - 340), any(Resolution.y - 310), "Powoli opadasz w kierunku ziemii. Masz 20 sekund.");
local draw2 = Draw(anx(Resolution.x/2 - 340), any(Resolution.y - 270), "Kliknij w dane miejsce na mapie aby zacz��.");
local draw3 = Draw(anx(Resolution.x/2 - 340), any(Resolution.y - 230), "Gdy dotkniesz ziemii zobaczysz innych graczy.");
local draw4 = Draw(anx(Resolution.x/2 - 340), any(Resolution.y - 190), "Co gorsza b�dziesz nara�ony odrazu na atak.");
local draw5 = Draw(anx(Resolution.x/2 - 340), any(Resolution.y - 150), "Pozosta�o 20 sekund/y do l�dowania.");

draw1.font = "FONT_OLD_10_WHITE_HI.TGA";
draw2.font = "FONT_OLD_10_WHITE_HI.TGA";
draw3.font = "FONT_OLD_10_WHITE_HI.TGA";
draw4.font = "FONT_OLD_10_WHITE_HI.TGA";
draw5.font = "FONT_OLD_10_WHITE_HI.TGA";

function LandingGUI::show()
{
    mapTexture.visible = true;

	local world = getWorld()

	switch (world)
	{
		case "NEWWORLD\\NEWWORLD.ZEN":
			mapTexture.file = "Map_NewWorld.tga"
            setLandingLevelCoords(-28000, 50500, 95500, -42500)
			break

		case "OLDWORLD\\OLDWORLD.ZEN":
			mapTexture.file = "Map_OldWorld.tga"
            setLandingLevelCoords(-78500, 47500, 54000, -53000)
			break

		case "ADDON\\ADDONWORLD.ZEN":
			mapTexture.file = "Map_AddonWorld.tga"
            setLandingLevelCoords(-47783, 36300, 43949, -32300)
			break
	}

    texture.visible = true;
    selectorPosition.visible = true;
    draw1.visible = true;
    draw2.visible = true;
    draw3.visible = true;
    draw4.visible = true;
    draw5.visible = true;

    local playerPosition = getPlayerPosition(heroId)

    playerPosition.x -= levelCords.x
    playerPosition.z -= levelCords.y

    local maxX = levelCords.width - levelCords.x
    local maxY = levelCords.height - levelCords.y

    playerPosition.x = (playerPosition.x / maxX.tofloat()) * 8192
    playerPosition.z = (playerPosition.z / maxY.tofloat()) * 8192

    selectorPosition.setPosition(playerPosition.x - anx(24),playerPosition.z - any(28))

    BaseGUI.show();

    Player.GUI = PlayerGUIEnum.Landing;

    check = getTickCount() + 1000;
    timer = 20;
}

function LandingGUI::hide()
{
    mapTexture.visible = false;
    texture.visible = false;
    selectorPosition.visible = false;
    draw1.visible = false;
    draw2.visible = false;
    draw3.visible = false;
    draw4.visible = false;
    draw5.visible = false;

    local pos = selectorPosition.getPosition();

    pos.x = pos.x + anx(24)
    pos.y = pos.y + any(28)

    local maxX = levelCords.width - levelCords.x
    local maxY = levelCords.height - levelCords.y

    pos.x = (pos.x * maxX.tofloat()) / 8192
    pos.y = (pos.y * maxY.tofloat()) / 8192

    pos.x += levelCords.x
    pos.y += levelCords.y

    setPlayerPosition(heroId, pos.x, 8000, pos.y)

    BaseGUI.hide();
    Player.GUI = false;

    DesktopGameGUI.show();

    addEffect(heroId,"VOB_BURN_CHILD1");
}

function LandingGUI::onRender()
{
    if(Player.GUI != PlayerGUIEnum.Landing)
        return;

    if(check > getTickCount())
        return;

    timer = timer - 1;

    if(timer == 1)
        draw5.text = "Pozosta�a 1 sekunda do l�dowania.";
    else if(timer < 5)
        draw5.text = "Pozosta�y "+timer+" sekundy do l�dowania.";
    else
        draw5.text = "Pozosta�o "+timer+" sekund do l�dowania.";

    if(timer <= 0)
        LandingGUI.hide();

    check = getTickCount() + 1000;
}

addEventHandler("onMouseClick", function (btn) {
    if(Player.GUI != PlayerGUIEnum.Landing)
        return;

    local posCursor = getCursorPosition();

    if(posCursor.x > 800 && posCursor.x < 7600 && posCursor.y > 800 && posCursor.y < 7600)
        selectorPosition.setPosition(posCursor.x - anx(24),posCursor.y - any(28))
})