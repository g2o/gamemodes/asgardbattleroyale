addPacketListener(Packets.Ranking, function(packet)
{
    switch(packet.readUInt8())
    {
        case RankingPackets.SendRanking:
            local rankingLen = packet.readInt16();

            for(local i = 0; i < rankingLen; i ++)
            {
                RankingGUI.addRanking(packet.readString(),
                packet.readInt16(),
                packet.readInt16(),
                packet.readInt16(),
                packet.readInt16(),
                packet.readInt16())
            }
        break;
        case RankingPackets.SendOnline:
            local rankingLen = packet.readInt16();
            for(local i = 0; i < rankingLen; i ++)
            {
                RankingGUI.addOnline(packet.readInt16(),
                packet.readString(),
                packet.readBool(),
                packet.readInt16(),
                packet.readInt16(),
                packet.readInt16(),
                packet.readInt16(),
                packet.readInt16())
            }
        break;
    }
})