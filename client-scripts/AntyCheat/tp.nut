PositionChangeDetector <- {};

local lastCheck = null;

function PositionChangeDetector::work() 
{
    local pos = getPlayerPosition(heroId);  
    if(lastCheck == null)
    {
        lastCheck = pos;
        return;
    }

    if(Game.timer > 100 && Game.timer < 3500) 
    {
        if(getDistance2d(pos.x, pos.z, lastCheck.x, lastCheck.z) > 1000)
        {
            local packet = Packet();
            packet.writeUInt8(Packets.AntyCheat);
            packet.writeUInt8(AntyCheatPackets.Tp);
            packet.send(RELIABLE_ORDERED);        
        }
    }

    lastCheck = pos;
}

setTimer(PositionChangeDetector.work, 500, 0);