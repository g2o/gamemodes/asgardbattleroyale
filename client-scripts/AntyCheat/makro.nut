
enum MakroDetectStatus 
{
    SAFE,
    WARNING,
    DANGER,
}

MakroDetector <- {
    CollectedHits = [],
    CollectedDelays = [],

    LastHit = getTickCount(),
};

function MakroDetector::run() {
    if(MakroDetector.CollectedHits.len() < 2)
        return;

    local firstStep = MakroDetector.recognizeBasicPattern();
    if(firstStep >= MakroDetectStatus.WARNING)
        MakroDetector.send(0, firstStep);
    
    local secondStep = MakroDetector.recognizeSecondPattern();
    if(secondStep >= MakroDetectStatus.WARNING)
        MakroDetector.send(1, secondStep);

    local thirdStep = MakroDetector.recognizeThirdPattern();
    if(thirdStep >= MakroDetectStatus.WARNING)
        MakroDetector.send(2, thirdStep);
        
}

function MakroDetector::GetData() {
    local key = AntyCheatHelpers.getLastKey();
    AntyCheatHelpers.addKeyToListening(key);
    local keyTime = AntyCheatHelpers.getLastKeyTime(key);

    if(keyTime != -1)
        MakroDetector.CollectedHits.append(keyTime);

    local currentTick = getTickCount();

    if(MakroDetector.LastHit + 1000 > currentTick)
        MakroDetector.CollectedDelays.append(currentTick - MakroDetector.LastHit); 
    
    MakroDetector.LastHit = currentTick;

    MakroDetector.run();
}

function MakroDetector::recognizeBasicPattern() {
    local danger = 30;
    local warning = 50;

    local our = 0;

    foreach(dataItem in MakroDetector.CollectedHits)
        our = our + dataItem;

    local our = our/MakroDetector.CollectedHits.len();
    if(our <= danger)
        return MakroDetectStatus.DANGER;
    else if(our <= warning)
        return MakroDetectStatus.WARNING;

    return MakroDetectStatus.SAFE;    
}

function MakroDetector::recognizeSecondPattern() {
    local detectedPatterns = {};

    foreach(v in MakroDetector.CollectedHits)
    {
        if(v in detectedPatterns)
            detectedPatterns[v] = detectedPatterns[v] + 1;
        else
            detectedPatterns[v] <- 0;
    }

    if(MakroDetector.CollectedHits.len() > 7)
    {
        local minDanger = MakroDetector.CollectedHits.len()/2 + 1;
        local minWarning = MakroDetector.CollectedHits.len()/2 - 1;
        foreach(value in detectedPatterns)
            if(value >= minDanger)
                return MakroDetectStatus.DANGER;
        
        foreach(value in detectedPatterns)
            if(value >= minWarning)
                return MakroDetectStatus.WARNING;
    }

    return MakroDetectStatus.SAFE;
}

function MakroDetector::recognizeThirdPattern() 
{
    if(MakroDetector.CollectedDelays.len() > 4)
    {
        local sumDelays = 0;
        local minDelay = 3000;
        local maxDelay = 0;

        foreach(v in MakroDetector.CollectedDelays)
        {
            sumDelays += v;

            if(v < minDelay)
                minDelay = v;

            if(v > maxDelay)
                maxDelay = v;
        } 

        local currT = sumDelays/MakroDetector.CollectedDelays.len();
        if(AntyCheatHelpers.isComparableIntegers(currT, minDelay, 2) && AntyCheatHelpers.isComparableIntegers(maxDelay, currT, 2))
            return MakroDetectStatus.DANGER;
        if(AntyCheatHelpers.isComparableIntegers(currT, minDelay, 8) && AntyCheatHelpers.isComparableIntegers(maxDelay, currT, 8))
            return MakroDetectStatus.WARNING;
        
        return MakroDetectStatus.SAFE;
    }
    return MakroDetectStatus.SAFE;
}

function MakroDetector::send(id, value) {
    local packet = Packet();
    packet.writeUInt8(Packets.AntyCheat);
    packet.writeUInt8(AntyCheatPackets.Makro);
    packet.writeUInt8(id);
    packet.writeUInt8(value);
    packet.send(RELIABLE_ORDERED);
}
