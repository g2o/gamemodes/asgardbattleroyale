
class ItemIntegration
{
    _Items = {};

    function addItem(instance, item)
    {
        local id = Items.id(instance);
        ItemIntegration._Items[id] <- Item(instance,item);
        return ItemIntegration._Items[id];
    }

    function getItem(id)
    {
        if(!ItemIntegration._Items.rawin(id))
            return null;

        return ItemIntegration._Items[id];
    }

    function packetEqUse(id)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Items);
        packet.writeUInt8(ItemPackets.EqUse);
        packet.writeInt16(id);
        packet.send(RELIABLE_ORDERED);
    }

    function packetEquipItem(id)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Items);
        packet.writeUInt8(ItemPackets.Equip);
        packet.writeInt16(id);
        packet.send(RELIABLE_ORDERED);
    }

    function packetUnEquipItem(id)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Items);
        packet.writeUInt8(ItemPackets.UnEquip);
        packet.writeInt16(id);
        packet.send(RELIABLE_ORDERED);
    }

    function packetUseItem(id, amount)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Items);
        packet.writeUInt8(ItemPackets.UseItem);
        packet.writeInt16(id);
        packet.writeInt16(amount);
        packet.send(RELIABLE_ORDERED);
    }

    function onPacket(packet)
    {
        local id = packet.readUInt8();
        if(id != Packets.Items)
            return;

        id = packet.readUInt8();
        switch(id)
        {
            case ItemPackets.Equip:
                local idItem = packet.readInt16();
                unEquipItemTable(idItem);
            break;
            case ItemPackets.UnEquip:
                local idItem = packet.readInt16();
                equipItemTable(idItem);
            break;
            case ItemPackets.EqUse:
                PlayerGUI.hide();
                PlayerGUI.show();
            break;
        }
    }
}

addEventHandler("onPacket", ItemIntegration.onPacket);



function addItem(instance, item)
{
    instance = instance.toupper();
    return ItemIntegration.addItem(instance, item);
}