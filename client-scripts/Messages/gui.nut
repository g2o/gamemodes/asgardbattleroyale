
MessageGUI <- {
    items = [],
    containers = [],

    visible = false,
}

function MessageGUI::newMessageItemStorage(id)
{
    local newItem = MessageGUI.Item(id);
    MessageGUI.items.append(newItem);

    if(MessageGUI.visible)
        newItem.show();
}

function MessageGUI::newMessageContainerStorage(id)
{
    local newItem = MessageGUI.Container(id);
    MessageGUI.containers.append(newItem);

    if(MessageGUI.visible)
        newItem.show();
}

function MessageGUI::newMessageStorage(id, message)
{
    foreach(item in MessageGUI.items)
        if(item.mid == id)
            item.addNewMessage(message);
}

function MessageGUI::newMessageStorageContainer(id, message)
{
    foreach(item in MessageGUI.containers)
        if(item.mid == id)
            item.addNewMessage(message);    
}

function MessageGUI::hideOut()
{
    foreach(item in MessageGUI.items)
        item.hide();

    foreach(container in MessageGUI.containers)
        container.hide();

    MessageGUI.visible = false;
}

function MessageGUI::showOut() {
    foreach(item in MessageGUI.items)
        item.show();

    foreach(container in MessageGUI.containers)
        container.show();

    MessageGUI.visible = true;
}


local lastOpenedMenu = -1;

addEventHandler("GUI.onClick", function(self)
{
    foreach(index, container in MessageGUI.containers)
    {
        if(container.button == self)
        {
            Messages[container.mid].minimalized = false;
            container.destroy();
            MessageGUI.newMessageItemStorage(container.mid);
            foreach(index2, container2 in MessageGUI.containers)
            {
                if(index <= index2)
                    continue;

                container2.forceBack();
            }
            setContainersAxisBack();
            MessageGUI.containers.remove(index);
            break;
        }     
    }
    foreach(index, item in MessageGUI.items)
    {
        if(item.close == self)
        {
            Messages[item.mid].closed = true;
            item.destroy();
            MessageGUI.items.remove(index);
            return;
        }
        if(item.minimize == self)
        {
            Messages[item.mid].minimalized = true;
            item.destroy();
            MessageGUI.items.remove(index);
            MessageGUI.newMessageContainerStorage(item.mid);
            return;
        }

        if(item.inputChat == self)
        {
            lastOpenedMenu = index;

            return false;
        }

        if(item.submit == self)
        {
            if(item.inputChat.getText().len() > 1) {
                local packet = Packet();
                packet.writeUInt8(Packets.Message);
                packet.writeUInt8(MesssagePackets.Send);
                packet.writeInt16(item.mid);
                packet.writeString(item.inputChat.getText());
                packet.send(RELIABLE_ORDERED);
                Messages[item.mid].addMessage(item.inputChat.getText(), MessageType.Sended);
                lastOpenedMenu = -1;
                item.inputChat.setText("");
                item.inputChat.setActivity(false);
            }
        }
    }
})

addEventHandler("onKey", function (key) {
    if(key == KEY_RETURN && lastOpenedMenu != -1)
    {
        local item = MessageGUI.items[lastOpenedMenu];
        if(item.inputChat.getText().len() > 1) {
            local packet = Packet();
            packet.writeUInt8(Packets.Message);
            packet.writeUInt8(MesssagePackets.Send);
            packet.writeInt16(item.mid);
            packet.writeString(item.inputChat.getText());
            packet.send(RELIABLE_ORDERED);
            Messages[item.mid].addMessage(item.inputChat.getText(), MessageType.Sended);
            lastOpenedMenu = -1;
            item.inputChat.setActivity(false);
            item.inputChat.setText("");
        }
    }
})