local LastAxisY = 7800;
local LastAxisX = 5000;

function setContainersAxisBack()
{
    LastAxisX = LastAxisX - 1000;
    if(LastAxisX < 5000)
    {
        LastAxisY = LastAxisY + 220;
        LastAxisX = 5000;
    } 
}

class MessageGUI.Container
{
    mid = -1;

    button = null;
    counter = 0;
    textButton = "";

    constructor(mid)
    {
        this.mid = mid;

        textButton = getPlayerName(mid);
        textButton = StringLib.textWrap(textButton, 10)[0];

        button = GUI.Button(LastAxisX, LastAxisY, 1100, 240, "INPUT_ENDLESS.TGA", textButton);

        LastAxisX = LastAxisX + 1000;
        if(LastAxisX > 8000) {
            LastAxisY = LastAxisY - 220;
            LastAxisX = 5000;
        }
    }

    function forceBack()
    {
        local pos = button.getPosition();
        pos.x = pos.x - 1000;
        if(pos.x < 5000)
        {
            pos.y = pos.y + 220;
            pos.x = 5000;
        }
        button.setPosition(pos.x, pos.y);
    }

    function addNewMessage(messageObj)
    {
        counter = counter + 1;
        button.setText("("+counter+") "+textButton);
    }

    function show()
    {
        button.setVisible(true);
    }

    function hide()
    {
        button.setVisible(false);
    }

    function destroy()
    {
        button.setVisible(false);
        button.destroy();
    }
}