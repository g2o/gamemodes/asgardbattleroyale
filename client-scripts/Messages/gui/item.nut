local LastAxisY = 3500;

class MessageGUI.Item
{
    mid = -1;

    window = null;
    grid = null;
    gridColumn = null;

    inputChat = null;
    submit = null;

    minimize = null;
    close = null;

    constructor(mid)
    {
        this.mid = mid;

        window = GUI.Window(anx(Resolution.x - 450), LastAxisY, anx(450), any(330), "MAIN_ENDLESS.TGA", null, true)

        GUI.Button(anx(60), any(0), anx(275), any(50), "INPUT_ENDLESS.TGA", getPlayerName(mid), window);
        minimize = GUI.Button(anx(390), any(0), anx(25), any(50), "INPUT_ENDLESS.TGA", "_", window);
        close = GUI.Button(anx(415), any(0), anx(25), any(50), "INPUT_ENDLESS.TGA", "X", window);

        grid = GUI.GridList(anx(5), any(50), anx(440), any(250), "SLOT_ENDLESS.TGA", "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", window);
        gridColumn = grid.addColumn("", anx(480), Align.Left);
        grid.setMarginPx(15,45,15,45);

        inputChat = GUI.Input(anx(0),any(305),anx(400),any(40), "INPUT_ENDLESS.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 30, window);
        submit = GUI.Button(anx(400),any(305),anx(60),any(40), "BUTTON_ENDLESS.TGA", ">", window);

        foreach(messageObj in Messages[mid].messages) {
            foreach(value in MessageGUI.Message(messageObj)) {
                local row = grid.addRow(value);   
                if(messageObj.typeId == MessageType.Received) {
                    foreach(cell in row.cells)
                        cell.draw.setColor(200, 150, 0);
                }else{
                    foreach(cell in row.cells)
                        cell.draw.setColor(0, 255, 155);
                }
            }
        }

        LastAxisY = LastAxisY - 500;
        if(LastAxisY < 500)
            LastAxisY = 3500;
    }

    function addNewMessage(messageObj)
    {
        foreach(value in MessageGUI.Message(messageObj)) {
            local row = grid.addRow(value);   
            if(messageObj.typeId == MessageType.Received) {
                foreach(cell in row.cells)
                    cell.draw.setColor(200, 150, 0);
            }else{
                foreach(cell in row.cells)
                    cell.draw.setColor(0, 255, 155);
            }
        }
    }

    function show()
    {
        window.setVisible(true);
    }

    function hide()
    {
        window.setVisible(false);
    }

    function destroy()
    {
        window.setVisible(false);
        window.destroy();
    }
}