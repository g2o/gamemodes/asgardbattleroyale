Messages <- {};

class MessageStorage
{
    mid = -1;
    messages = null;

    minimalized = false;
    closed = false;

    /**
    @param mid - user that connect with message
    */
    constructor(mid) {
        this.mid = mid;
        this.messages = [];

        Messages[mid] <- this;
        MessageGUI.newMessageItemStorage(mid);
    }

    function addMessage(message, typeId) {
        local data = mDate.get().smallStr;
        messages.append(Message(message, typeId, data));

        if(minimalized == false && closed == false)
            MessageGUI.newMessageStorage(mid, messages[messages.len()-1]);
        else if(minimalized == true && closed == false)
            MessageGUI.newMessageStorageContainer(mid, messages[messages.len()-1]);
    }

    function maximizeAndOpen() {
        if(closed == true)
            MessageGUI.newMessageItemStorage(mid);

        minimalized = false;
        closed = false;    
    }

    static function onPlayerDestroy(id)
    {
        Messages[id] <- null;
    }
}

