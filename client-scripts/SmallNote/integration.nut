addPacketListener(Packets.SmallNotes, function(packet)
{
    switch(packet.readUInt8())
    {
        case SmallNotesPackets.Add:
            local typeId = packet.readUInt8();
            switch(typeId)
            {
                case NoteType.Damage:
                    SmallNote(typeId, packet.readString(), packet.readInt16());
                break;
                case NoteType.Information:
                    SmallNote(typeId, packet.readString(), -1);
                break;
                case NoteType.Kill:
                    SmallNote(typeId, packet.readString());
                break;
            }
        break;
    }
})