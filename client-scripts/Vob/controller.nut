local lineToGameCircle = [];
local vobsToGameCircle = [];
local check = -1;

VobController <- {
    "mapCircle" : function () {
        lineToGameCircle.clear();
        vobsToGameCircle.clear();

        local pos = getPlayerPosition(heroId);
        foreach(index, value in Game.circle.polyX)
		{
            local nextX = 0;
            local nextZ = 0;
            if(index == Game.circle.polyX.len()-1) {
                nextX = Game.circle.polyX[0];
                nextZ = Game.circle.polyZ[0];
            }else {
                nextX = Game.circle.polyX[index + 1];
                nextZ = Game.circle.polyZ[index + 1];
            }

            lineToGameCircle.append(Line3d(Game.circle.polyX[index],pos.y,Game.circle.polyZ[index], nextX, pos.y, nextZ));
            lineToGameCircle[lineToGameCircle.len()-1].setColor(200,0,0);

            local begin = lineToGameCircle[lineToGameCircle.len()-1].getBegin();
            if(getDistance2d(pos.x, pos.z, begin.x, begin.z) < 4000) {
                lineToGameCircle[lineToGameCircle.len()-1].visible = true;
                vobsToGameCircle.append(Vob("EVT_SPAWN_01.3DS"));
                vobsToGameCircle[vobsToGameCircle.len()-1].addToWorld();
                vobsToGameCircle[vobsToGameCircle.len()-1].setPosition(Game.circle.polyX[index],pos.y-50,Game.circle.polyZ[index]);
                vobsToGameCircle.append(Vob("EVT_SPAWN_01.3DS"));
                vobsToGameCircle[vobsToGameCircle.len()-1].addToWorld();
                vobsToGameCircle[vobsToGameCircle.len()-1].setPosition(Game.circle.polyX[index],pos.y,Game.circle.polyZ[index]);
                vobsToGameCircle.append(Vob("EVT_SPAWN_01.3DS"));
                vobsToGameCircle[vobsToGameCircle.len()-1].addToWorld();
                vobsToGameCircle[vobsToGameCircle.len()-1].setPosition(Game.circle.polyX[index],pos.y+50,Game.circle.polyZ[index]);
            }
        }
    },

    "clearVobs": function() {
        lineToGameCircle.clear();
        vobsToGameCircle.clear();
    },

    "onRender" : function () {
        if(check > getTickCount())
            return;
            
        if(lineToGameCircle.len() == 0)
            return;

        local pos = getPlayerPosition(heroId);
        foreach(line in lineToGameCircle)
        {
            local begin = line.getBegin();

            if(getDistance2d(pos.x, pos.z, begin.x, begin.z) < 3000)
                line.visible = true;
            else
                line.visible = false;

            local end = line.getEnd();
            line.setBegin(begin.x, pos.y, begin.z);
            line.setEnd(end.x, pos.y, end.z);
        }

        check = getTickCount() + 1000;
    }
}