addPacketListener(Packets.Game, function(packet)
{
    switch(packet.readUInt8())
    {
        case GamePackets.Enter:
            local statusGame = packet.readUInt8();
            local isAdmin = packet.readBool();
            switch(statusGame)
            {
                case GameStatus.Game:
                    Game.Controller.setMapStartPoint(packet.readFloat(), packet.readFloat(), packet.readFloat());
                    Game.activeWorld = packet.readString();
                break;
            }
            
            Game.Controller.onEnter(heroId, statusGame, isAdmin)
        break;
        case GamePackets.StartGame:
            Game.Controller.startGame(packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readString());
        break;
        case GamePackets.Timer:
            Game.Controller.setGameTimer(packet.readInt16(), packet.readInt16());
        break;
        case GamePackets.EndGame:
            Game.Controller.endGame();
        break;
        case GamePackets.ResetState:
            if(Player.GUI == PlayerGUIEnum.Observer)
                Observer.hide();
        break;
    }
})