
PlayerGUI <- {};
local Resolution = getResolution();

PlayerGUI.window <- GUI.Window(anx(Resolution.x - 700), any(Resolution.y/2 - 380), anx(600), any(500), "MAIN_ENDLESS.TGA", null, false); GUI.Texture(anx(250), any(50), anx(300), any(400), "EQ_ENDLESS.TGA", PlayerGUI.window);
PlayerGUI.textures <- [];
PlayerGUI.draws <- [];
PlayerGUI.items <- [];
PlayerGUI.itemTextures <- [];
PlayerGUI.scroll <- GUI.ScrollBar(anx(50), any(50), anx(30), any(600), "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", Orientation.Vertical);

PlayerGUI.itemBackground <- GUI.Window(anx(50), any(Resolution.y/2 + 200), anx(700), any(200), "FRAME_HERO_ENDLESS.TGA", null, false);
local drawInstance = [];

local NAV_ITEMS = 0;

PlayerGUI.startPos <- {x = anx(Resolution.x - 700), y = any(Resolution.y/2 - 380)}

function PlayerGUI::show() {
    BaseGUI.show();
    DesktopGameGUI.hideOut();

    PlayerGUI.window.setVisible(true);
    Player.GUI = PlayerGUIEnum.Statistics;

    PlayerGUI.View.ShowName();
    PlayerGUI.View.ShowDeaths();
    PlayerGUI.View.ShowWinGames();
    PlayerGUI.View.ShowKills();
    PlayerGUI.View.ShowArmor();
    PlayerGUI.View.ShowMeleeWeapon();
    PlayerGUI.View.ShowRangedWeapon();
    PlayerGUI.View.ShowHealth();
    PlayerGUI.View.ShowClan();
    PlayerGUI.View.ShowInformation();
    PlayerGUI.View.ShowEquipment();

    PlayerGUI.itemBackground.setVisible(false);
    PlayerGUI.scroll.setVisible(true);
    PlayerGUI.scroll.setValue(0);

    foreach(item in PlayerGUI.items)
        item.button.setVisible(true);

    foreach(texture in PlayerGUI.itemTextures) {
        texture.visible = true;
        texture.top();
    }

    foreach(item in PlayerGUI.items)
        item.button.draw.top();

    foreach(texture in PlayerGUI.textures)
        texture.visible = true;

    foreach(draw in PlayerGUI.draws)
        draw.visible = true;
}

function PlayerGUI::hide() {
    BaseGUI.hide();
    DesktopGameGUI.showOut();

    PlayerGUI.window.setVisible(false);
    PlayerGUI.itemBackground.setVisible(false);
    Player.GUI = false;

    PlayerGUI.scroll.setVisible(false);

    foreach(item in PlayerGUI.items)
        item.button.destroy();

    PlayerGUI.draws.clear();
    drawInstance.clear();
    PlayerGUI.items.clear();
    PlayerGUI.textures.clear();
    PlayerGUI.itemTextures.clear();
}

function PlayerGUI::showItemDescription(instance)
{
    PlayerGUI.itemBackground.setVisible(true);
    local obj = ItemIntegration.getItem(Items.id(instance));
    if(obj)
    {
        switch(obj.flag)
        {
            case ITEM_KAT_ARMOR:
                drawInstance = [
                    Draw(anx(100), any(Resolution.y/2 + 220), obj.name),
                    Draw(anx(100), any(Resolution.y/2 + 280), obj.description),
                    Draw(anx(100), any(Resolution.y/2 + 310), "Ochrona: " +obj.protection),
                    Draw(anx(100), any(Resolution.y/2 + 330), "Ochrona przed magi�: "+obj.magicProtection),
                ];
            break;
            case ITEM_KAT_MELEE: case ITEM_KAT_RANGED: case ITEM_KAT_RUNE:
                drawInstance = [
                    Draw(anx(100), any(Resolution.y/2 + 220), obj.name),
                    Draw(anx(100), any(Resolution.y/2 + 280), obj.description),
                    Draw(anx(100), any(Resolution.y/2 + 310), "Obra�enia: " +obj.damage),
                    Draw(anx(100), any(Resolution.y/2 + 330), "Obra�enia magiczne: "+obj.magicDamage),
                ];
            break;
            case ITEM_KAT_OTHER:
                drawInstance = [
                    Draw(anx(100), any(Resolution.y/2 + 220), obj.name),
                    Draw(anx(100), any(Resolution.y/2 + 280), obj.description),
                ];
            break;
        }
        drawInstance[0].font = "FONT_OLD_20_WHITE_HI.TGA";
        foreach(draw in drawInstance)
            draw.visible = true;
    }
}

addEventHandler("onInit", function () {
    bindKey(KEY_I, PlayerGUI.show)
    bindKey(KEY_I, PlayerGUI.hide, PlayerGUIEnum.Statistics)
    bindKey(KEY_TAB, PlayerGUI.show)
    bindKey(KEY_TAB, PlayerGUI.hide, PlayerGUIEnum.Statistics)
    bindKey(KEY_ESCAPE, PlayerGUI.hide, PlayerGUIEnum.Statistics)
})

class PlayerGUI.View
{
    function ShowName()
    {
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(70), getPlayerName(heroId));
        draw.font = "FONT_OLD_20_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowDeaths()
    {
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(170), "�mierci: "+Player.deaths);
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowWinGames()
    {
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(140), "Wygrane Gry: "+Player.winGames);
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowKills()
    {
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(110), "Zab�jstwa: "+Player.kills);
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowHealth()
    {
        local hpPercent = abs((getPlayerHealth(heroId) * 100)/getPlayerMaxHealth(heroId));
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(210), "Zdrowie: "+hpPercent+"%");
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowClan()
    {
        local clanName = "Brak";
        if(getPlayerClan(heroId) != -1)
            clanName = Clan.List[getPlayerClan(heroId)].name;

        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(240), "Klan: "+clanName);
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowInformation()
    {
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(300), "Skill Z: podwaja dmg z ka�dego uderzenia.");
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(330), "Skill X: przebijaj�ce obron� uderzenie.");
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(360), "Skill C: przyspiesza ci� na chwil�.");
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(390), "Skill V: tarcza kontruje 2 razy skille.");
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
        local draw = Draw(PlayerGUI.startPos.x + anx(50), PlayerGUI.startPos.y + any(420), "Skill B: na 5 uderze� bonus do obra�e�.");
        draw.font = "FONT_OLD_10_WHITE_HI.TGA";
        PlayerGUI.draws.append(draw);
    }

    function ShowArmor()
    {
        local armor = getPlayerArmor(heroId);
        if(armor != -1)
            PlayerGUI.textures.append(ItemRender(PlayerGUI.startPos.x + anx(300), PlayerGUI.startPos.y + any(80), anx(200), any(240), Items.name(armor)));
    }

    function ShowMeleeWeapon()
    {
        local weapon = getPlayerMeleeWeapon(heroId);
        if(weapon != -1) {
            PlayerGUI.textures.append(ItemRender(PlayerGUI.startPos.x + anx(200), PlayerGUI.startPos.y + any(40), anx(200), any(240), Items.name(weapon)));
            PlayerGUI.textures[PlayerGUI.textures.len()-1].rotX = -85;
            PlayerGUI.textures[PlayerGUI.textures.len()-1].rotZ = -60;
        }
    }

    function ShowRangedWeapon()
    {
        local weapon = getPlayerRangedWeapon(heroId);
        if(weapon != -1) {
            PlayerGUI.textures.append(ItemRender(PlayerGUI.startPos.x + anx(400), PlayerGUI.startPos.y + any(40), anx(200), any(240), Items.name(weapon)));
            PlayerGUI.textures[PlayerGUI.textures.len()-1].rotX = -75;
            PlayerGUI.textures[PlayerGUI.textures.len()-1].rotZ = -215;
        }
    }

    function ShowEquipment()
    {
        PlayerGUI.itemTextures.clear();

        local leftRightIndex = 0;
        local getEqList = getEq();

        getEqList.sort(function (firstItem, secondItem) {
            local valueFirstItem = 0, valueSecondItem = 0;

            valueFirstItem = ItemIntegration.getItem(Items.id(firstItem.instance)).flag;
            valueSecondItem = ItemIntegration.getItem(Items.id(secondItem.instance)).flag;

            if(valueSecondItem == 7)
                valueSecondItem = 3;

            if(valueFirstItem == 7)
                valueFirstItem = 3;

            if(valueFirstItem == valueSecondItem)
            {
                switch(valueFirstItem)
                {
                    case ITEM_KAT_MELEE: case ITEM_KAT_RANGED: case ITEM_KAT_RUNE:
                        valueFirstItem = valueFirstItem - ItemIntegration.getItem(Items.id(firstItem.instance)).damage;
                    break;
                    case ITEM_KAT_ARMOR:
                        valueFirstItem = valueFirstItem - ItemIntegration.getItem(Items.id(firstItem.instance)).getSumProtection();
                    break;
                }
                switch(valueSecondItem)
                {
                    case ITEM_KAT_MELEE: case ITEM_KAT_RANGED: case ITEM_KAT_RUNE:
                        valueSecondItem = valueSecondItem - ItemIntegration.getItem(Items.id(secondItem.instance)).damage;
                    break;
                    case ITEM_KAT_ARMOR:
                        valueSecondItem = valueSecondItem - ItemIntegration.getItem(Items.id(secondItem.instance)).getSumProtection();
                    break;
                }
            }

            if (valueFirstItem > valueSecondItem) return 1;
            if (valueFirstItem < valueSecondItem) return -1;
            return 0;
        });

        foreach(index, item in getEqList)
        {
            if(index >= (NAV_ITEMS * 4) && index < (NAV_ITEMS * 4) + 12)
            {
                if(leftRightIndex > 3)
                    leftRightIndex = 0;

                local topBottomIndex = 0;
                if(PlayerGUI.itemTextures.len() > 3 && PlayerGUI.itemTextures.len() <= 7)
                    topBottomIndex = 1;
                else if(PlayerGUI.itemTextures.len() > 7)
                    topBottomIndex = 2;

                PlayerGUI.itemTextures.append(ItemRender(anx(100) + anx(150*leftRightIndex), any(100 + 150 * topBottomIndex), anx(150), any(150), item.instance));
                PlayerGUI.items.append({ button = GUI.Button(anx(100) + anx(150*leftRightIndex), any(100 + 150 * topBottomIndex), anx(150), any(150), "FRAME_HERO_ENDLESS", item.amount), instance = item.instance});

                local itemId = Items.id(item.instance);
                if(PlayerItem.findInEquipment(itemId)) {
                    PlayerGUI.items[PlayerGUI.items.len()-1].button.setAlpha(200);
                    PlayerGUI.items[PlayerGUI.items.len()-1].button.draw.setColor(250,150,0);
                    PlayerGUI.items[PlayerGUI.items.len()-1].button.draw.setAlpha(250);
                }else {
                    PlayerGUI.items[PlayerGUI.items.len()-1].button.setAlpha(100);
                    PlayerGUI.items[PlayerGUI.items.len()-1].button.draw.setColor(250,150,0);
                    PlayerGUI.items[PlayerGUI.items.len()-1].button.draw.setAlpha(250);
                }

                leftRightIndex = leftRightIndex + 1;
            }
        }
    }
}

addEventHandler("GUI.onMouseIn", function(self) {
    if(Player.GUI != PlayerGUIEnum.Statistics)
        return;
    
    foreach(item in PlayerGUI.items)
    {
        if(item.button == self)
        {
            PlayerGUI.showItemDescription(item.instance);
        }
    }
})

addEventHandler("GUI.onChange", function (self) {
    if(Player.GUI != PlayerGUIEnum.Statistics)
        return;

    if( self == PlayerGUI.scroll )
    {
        foreach(item in PlayerGUI.items)
            item.button.destroy();

        PlayerGUI.items.clear();

        NAV_ITEMS = PlayerGUI.scroll.getValue();
        PlayerGUI.View.ShowEquipment();

        foreach(item in PlayerGUI.items)
            item.button.setVisible(true);

        foreach(texture in PlayerGUI.itemTextures) {
            texture.visible = true;
            texture.top();
        }

        foreach(item in PlayerGUI.items)
            item.button.draw.top();
    }
})

addEventHandler("GUI.onClick", function(self)
{
    if(Player.GUI != PlayerGUIEnum.Statistics)
        return;

    foreach(item in PlayerGUI.items)
    {
        if(item.button == self)
        {
            local itemId = Items.id(item.instance);
            local itemInstance = ItemIntegration.getItem(itemId);
            if(itemInstance == null)
                return;

            ItemIntegration.packetEqUse(itemId);
        }
    }
})
