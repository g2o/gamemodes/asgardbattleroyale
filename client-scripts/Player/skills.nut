local check = 0;
local MAX_TIMEOUTS = [120, 60, 45, 60, 120];

Player.Skill <- {
    timeouts = [0,0,0,0,0],
    active = [false,false,false,false,false],

    "onRender": function () {
        if(check > getTickCount())
            return;

        foreach(index, value in Player.Skill.timeouts)
        {
            if(value == 0)
                continue;

            Player.Skill.timeouts[index] = value - 1;

            if(value < 1)
                value = 0;
        }

        check = getTickCount() + 1000;
    }
}

function resetSkillTimeouts() {
    Player.Skill.timeouts[0] = 120;
    Player.Skill.timeouts[1] = 60;
    Player.Skill.timeouts[2] = 45;
    Player.Skill.timeouts[3] = 60;
    Player.Skill.timeouts[4] = 120;

    local check = getTickCount() + 1000;

    local packet = Packet();
    packet.writeUInt8(Packets.Player);
    packet.writeUInt8(PlayerPackets.ResetSkill);
    packet.send(RELIABLE_ORDERED);
}

function setPlayerSkillTimeouts(val1,val2,val3,val4,val5) 
{
    Player.Skill.timeouts[0] = val1;
    Player.Skill.timeouts[1] = val2;
    Player.Skill.timeouts[2] = val3;
    Player.Skill.timeouts[3] = val4;
    Player.Skill.timeouts[4] = val5;    
}

function onPlayerUseSkill(pid, skillId) 
{
    if(pid == heroId)
    {
        Player.Skill.timeouts[skillId] = MAX_TIMEOUTS[skillId];
    }

    switch(skillId)
    {
        case PlayerSkill.Arrows:
            addEffect(pid, "spellFX_Icebolt_COLLIDE");
            SmallNote(NoteType.Skill, getPlayerName(pid)+" u�y� podwojenia.", pid)
        break;
        case PlayerSkill.Shield:
            addEffect(pid, "spellFX_LIGHTSTAR_WHITE");
            SmallNote(NoteType.Skill, getPlayerName(pid)+" u�y� tarczy.", pid)
        break;
        case PlayerSkill.DeathKiss:
            addEffect(pid, "spellFX_ArmyOfDarkness");
            SmallNote(NoteType.Skill, getPlayerName(pid)+" u�y� przebicia.", pid)
        break;
        case PlayerSkill.Devil:
            addEffect(pid, "spellFX_Fear_WINGS2");
            addEffect(pid, "spellFX_Fear_WINGS");
            SmallNote(NoteType.Skill, getPlayerName(pid)+" u�y� mocy demona.", pid)
        break;
        case PlayerSkill.Flash:
            addEffect(pid, "spellFX_DestroyUndead");
            SmallNote(NoteType.Skill, getPlayerName(pid)+" u�y� przyspieszenia.", pid)
        break;
    }
}


addEventHandler("onKey", function (key) {
	if (chatInputIsOpen())
		return
		
	if (isConsoleOpen())
		return

    if(DesktopGameGUI.textures[0].visible == false)
        return;

    if(chatInputIsOpen() && isConsoleOpen())
        return;

    if(getPlayerHealth(heroId) < 1)
        return;

    if(key == KEY_Z)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.UseSkill);
        packet.writeUInt8(0);
        packet.send(RELIABLE_ORDERED);
    }
    else if(key == KEY_X)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.UseSkill);
        packet.writeUInt8(1);
        packet.send(RELIABLE_ORDERED);
    }
    else if(key == KEY_C)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.UseSkill);
        packet.writeUInt8(2);
        packet.send(RELIABLE_ORDERED);
    }
    else if(key == KEY_V)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.UseSkill);
        packet.writeUInt8(3);
        packet.send(RELIABLE_ORDERED);
    }
    else if(key == KEY_B)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Player);
        packet.writeUInt8(PlayerPackets.UseSkill);
        packet.writeUInt8(4);
        packet.send(RELIABLE_ORDERED);
    }
})