
addPacketListener(Packets.Bots, function(packet)
{
    switch(packet.readUInt8())
    {
        case BotPackets.Init:
            local id = packet.readInt16();
            local streamerId = packet.readInt16();
            local scheme = packet.readUInt8();

            local name = packet.readString();
            local animation = packet.readString();

            local posx = packet.readFloat();
            local posy = packet.readFloat();
            local posz = packet.readFloat();
            local angle = packet.readInt16();
            switch(scheme)
            {
                case BotScheme.Mount:
                    createMount(id, streamerId, name, posx, posy, posz, angle, packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readString(), packet.readInt16(), packet.readString());
                break;
                case BotScheme.Monster:
                    createMonster(id, streamerId, name, posx, posy, posz, angle);
                break;
                case BotScheme.Helper:
                    createHelper(id, streamerId, name, posx, posy, posz, angle, packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readString(), packet.readInt16(), packet.readString());
                break;
                case BotScheme.Boss:
                    createBoss(id, streamerId, name, posx, posy, posz, angle);
                break;
            }

            setBotAnimation(id, animation);
        break;
        case BotPackets.Streamer:
            local id = packet.readInt16();
            local streamerId = packet.readInt16();

            setBotStreamer(id, streamerId);
        break;
        case BotPackets.Health:
            local id = packet.readInt16();
            local health = packet.readInt16();
            setBotHealth(id, health);
            print(id)
        break;
        case BotPackets.Position:
            local id = packet.readInt16();

            local posx = packet.readFloat();
            local posy = packet.readFloat();
            local posz = packet.readFloat();

            setBotPosition(id, posx, posy, posz);
        break;
        case BotPackets.Angle:
            local id = packet.readInt16();
            local angle = packet.readInt16();

            setBotAngle(id, angle);
        break;
        case BotPackets.Remove:
            local id = packet.readInt16();
            removeBot(id);
        break;
        case BotPackets.Spawn:
            local id = packet.readInt16();
            local animation = packet.readString();

            local posx = packet.readFloat();
            local posy = packet.readFloat();
            local posz = packet.readFloat();
            local angle = packet.readInt16();

            local health = packet.readInt16();
            local maxHealth = packet.readInt16();

            local obj = getBot(id);

            obj.angle = angle;
            obj.position = {x = posx, y = posy, z = posz};
            obj.health = health;
            obj.maxHealth = maxHealth;
            obj.animation = animation;

            setBotSpawn(id);
        break;
        case BotPackets.WeaponMode:
            local id = packet.readInt16();
            local value = packet.readBool();

            local obj = getBot(id);
            obj.setWeaponMode(value);
        break;
        case BotPackets.Unspawn:
            local id = packet.readInt16();
            setBotUnspawn(id);
        break;
        case BotPackets.Animation:
            local id = packet.readInt16();
            local animation = packet.readString();

            setBotAnimation(id, animation);
        break;
        case BotPackets.Synchronization:
            local id = packet.readInt16();
            local posx = packet.readFloat();
            local posy = packet.readFloat();
            local posz = packet.readFloat();
            local angle = packet.readInt16();

            setBotPosition(id, posx, posy, posz, true);
            setBotAngle(id, angle);
        break;
        case BotPackets.Attack:
            local id = packet.readInt16();
            local targetid = packet.readInt16();
            local additionalData = -1;

            local bot = getBot(id);
            if(bot instanceof createHelper)
                additionalData = packet.readInt16();

            setBotAttack(id, targetid, additionalData);
        break;
        case BotPackets.BossAttack:
            local id = packet.readInt16();
            local targetId = packet.readInt16();
            local typeAttackId = packet.readUInt8();     

            local bot = getBot(id);
            bot.bossAttack(typeAttackId,targetId);       
        break;
    }
})