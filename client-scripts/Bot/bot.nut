
local All = {}, HeroStreams = [], Index = {}, ToDelete = [];

class Bot
{
    botId = -1;
    streamerId = -1;

    spawned = false;
    element = null;

    health = 40;
    maxHealth = 40;
    angle = 0;
    animation = "S_RUN";
    position = null;

    armor = -1;
    melee = -1;
    ranged = -1;

    headTxt = -1;
    headModel = "";
    bodyTxt = -1;
    bodyModel = "";

    weaponMode = false;

    constructor(id, streamer, name) 
    {    
        botId = id;
        streamerId = streamer;

        if(heroId == streamer)
            HeroStreams.append(botId);

        position = {x = 0, y = 0, z = 0};

        element = createNpc(name);

        Index[id] <- element;
        All[id] <- this;
    }

    function onTimer() {
        
    }

    function isDifferFromOriginalPosition() {
        local pos = getPlayerPosition(element);
        if(pos == null)
            return false;

        if(pos.x == 0 && pos.y == 0)
            return false;

        if(getDistance3d(pos.x, pos.y, pos.z, position.x, position.y, position.z) > 20) {
            position.x = pos.x;
            position.y = pos.y;
            position.z = pos.z;
            angle = getPlayerAngle(element);
            return true;
        }

        return false;
    }

    static function sendMyBots() 
    {
        local send = [];
        foreach(botId in HeroStreams)
        {
            local bot = All[botId];
            if(bot.isDifferFromOriginalPosition())
                send.append({bot_id = bot.botId, bot_x = bot.position.x, bot_y = bot.position.y, bot_z = bot.position.z, bot_angle = bot.angle})
        }

        if(send.len() == 0)
            return;

        local packet = Packet();
        packet.writeUInt8(Packets.Bots);
        packet.writeUInt8(BotPackets.Synchronization);
        packet.writeInt16(send.len());
        foreach(bot_send in send)
        {
            packet.writeInt16(bot_send.bot_id);
            packet.writeFloat(bot_send.bot_x);
            packet.writeFloat(bot_send.bot_y);
            packet.writeFloat(bot_send.bot_z);
            packet.writeInt16(bot_send.bot_angle);
        }
        packet.send(RELIABLE_ORDERED);
    }

    static function checkEveryBot() {
        foreach(bot in All)
            bot.onTimer();
    }
}

setTimer(Bot.sendMyBots, 300, 0);

function setBotStreamer(id, streamer) 
{
    All[id].streamerId = streamer;

    if(HeroStreams.find(id) != null)
        HeroStreams.remove(HeroStreams.find(id));

    if(heroId == streamer)
        HeroStreams.append(id);
}
function setBotHealth(id, value) 
{
    All[id].health = value;

    setPlayerHealth(Index[id], value);
}
function setBotPosition(id, posx, posy, posz, fromSynchro = false)
{
    if(fromSynchro)
    {
        local obj = All[id];
        local pos = getPlayerPosition(Index[id]);
        if(getDistance2d(pos.x, pos.z, obj.position.x, obj.position.z) <= 30)
            return;
    }

    All[id].position = {x = posx, y = posy, z = posz};
    setPlayerPosition(Index[id], posx, posy, posz);
}
function setBotAngle(id, angle)
{
    All[id].angle = angle;

    setPlayerAngle(Index[id], angle);
}
function setBotAnimation(id, value, send = false) {
    All[id].animation = value;

    if(value == "STOP")
        stopAni(Index[id]);
    else
        playAni(Index[id], value);

    if(send)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Bots);
        packet.writeUInt8(BotPackets.Synchronization);    
        packet.writeInt16(id);
        packet.writeString(value);
        packet.send(RELIABLE_ORDERED);    
    }
}
function setBotAttack(id, targetid, additional = 0) {
    if(isNpc(targetid))
        targetid = Index[targetid];

    playAni(Index[id], "S_RUN");
    stopAni(Index[id]);

    if(heroId == targetid)
    {
        if(getPlayerInstance(Index[id]) == "PC_HERO") {
            switch(additional)
            {
                case 1:
                    attackPlayer(Index[id], targetid, ATTACK_SWORD_LEFT);
                break;
                case 2:
                    attackPlayer(Index[id], targetid, ATTACK_SWORD_RIGHT);
                break;
                default:
                    attackPlayer(Index[id], targetid, ATTACK_SWORD_FRONT);
                break;
            }
        }else 
            attackPlayer(Index[id], targetid, ATTACK_FRONT);
    }else{
        if(getPlayerInstance(Index[id]) == "PC_HERO") {
            switch(additional)
            {
                case 1:
                    playAni(Index[id], "T_1HATTACKL");  
                break;
                case 2:
                    playAni(Index[id], "T_1HATTACKR");  
                break;
                default:
                    playAni(Index[id], "S_1HATTACK");  
                break;
            }
        }else 
            playAni(Index[id], "S_FISTATTACK");      
    }
}

function removeBot(id) 
{
    if(HeroStreams.find(id) != null)
        HeroStreams.remove(HeroStreams.find(id));   

    if(All[id] instanceof createMount) {
        removeMounting(All[id].mountedUserId);
        unspawnNpc(Index[id]);
    }else{
        ToDelete.append({timer = 10, id = Index[id]}); 
        setPlayerHealth(Index[id], 0); 
        playAni(Index[id], "T_DEATH"); 
    }  
      
    Index.rawdelete(id);
    All.rawdelete(id);
}

function Bot_onEndGame()
{
    foreach(_index, bot in All) {
        unspawnNpc(bot.element);
    }
    
    Index.clear();
    All.clear();
    HeroStreams.clear();
    ToDelete.clear();
}

function setBotSpawn(id) 
{
    spawnNpc(Index[id]);

    All[id].spawn();
}

function setBotUnspawn(id) 
{
    unspawnNpc(Index[id]);

    All[id].unspawn();
}

function getBot(id) {
    if(id in All)
        return All[id];
    
    return null;
}

function getIndexBots() {
    return Index;
}

function Bot_onPlayerHit(kid, pid, dmg)
{
    if(isPlayer(pid) && isPlayer(kid)) {
        return;
    }
    eventValue(0);

    if(pid == heroId)
    {
        local botId = -1;

        foreach(_botId, elementId in Index)
            if(elementId == kid)
                botId = _botId;

        if(botId == -1)
            return;

        local packet = Packet();
        packet.writeUInt8(Packets.Bots);
        packet.writeUInt8(BotPackets.Attack);
        packet.writeUInt8(1);
        packet.writeInt16(botId);
        packet.send(RELIABLE_ORDERED);
    }
    if (kid == heroId)
    {
        local botId = -1;

        foreach(_botId, elementId in Index)
            if(elementId == pid)
                botId = _botId;

        if(botId == -1)
            return;
        
        local packet = Packet();
        packet.writeUInt8(Packets.Bots);
        packet.writeUInt8(BotPackets.Attack);
        packet.writeUInt8(2);
        packet.writeInt16(botId);
        packet.send(RELIABLE_ORDERED);
    }
}

setTimer(function() {
    if(ToDelete.len() == 0)
        return;

    foreach(_ind, _val in ToDelete)
    {
        if(_val.timer <= 0)
        {
            unspawnNpc(_val.id);
            ToDelete.remove(_ind);
        }else{
            _val.timer = _val.timer - 1;
        }
    }
}, 1000, 0)