local list = [];

for(local i = 0; i < 10; i++)
    list.append(null);

function clearListLobbyChat() {
    for(local i = 0; i < 10; i++)
        list[i] = null;

    foreach(draw in LobbyGUI.ChatTable){
        draw.setVisible(false);
    }

    LobbyGUI.ChatList.destroy();
    LobbyGUI.ChatList = null;
    LobbyGUI.ChatList = GUI.Window(anx(50), any(Resolution.y - 400), anx(1200), any(400), "");
    LobbyGUI.InputChat = GUI.Input(anx(0),any(310),anx(900),any(60), "INPUT_ENDLESS.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 30, LobbyGUI.ChatList);
    LobbyGUI.Submit = GUI.Button(anx(950),any(300),anx(200),any(80), "BUTTON_ENDLESS.TGA", "Wy�lij", LobbyGUI.ChatList);

    LobbyGUI.ChatTable.clear();
}

function getListLobbyChat() {
    return list;
}

function addLobbyQuerie(name) {
    foreach(index, insert in list)
        if(index != 9)
            list[(9-index)] = list[(8-index)];

    list[0] = name;
    LobbyGUI.LoadChat();
}

function sendLobbyQuerie(name) {    
    local packet = Packet();
    packet.writeUInt8(Packets.Helpers);
    packet.writeUInt8(HelperPackets.AddLobby);
    packet.writeString(name);
    packet.send(RELIABLE_ORDERED);
}