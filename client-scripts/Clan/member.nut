class ClanMember
{
    id = -1;

    role = -1;
    clan_id = -1;

    constructor(id)
    {
        this.id = id;
    }
}

PlayerClan <- {};

for (local pid = 0; pid <= getMaxSlots(); ++pid)
    PlayerClan[pid] <- ClanMember(pid);


function setPlayerClan(pid, clanId) {
    PlayerClan[pid].clan_id = clanId;
}

function getPlayerClan(pid) {
    return PlayerClan[pid].clan_id;
}


