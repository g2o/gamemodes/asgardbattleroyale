local All = [];
local check = getTickCount() + 1000;
local wait = 0;
local timerLeftOut = 5;

class Teleport
{
    id = -1;

    location = null;
    teleport = null;
    goesIn = false;

    npcElement = null;

    constructor() {
        location = {};
        teleport = {};

        All.append(this);

        id = All.len() - 1;
    }

    function onPlayerEnterWorld(world) {

    }

    function setUpMob() {
        //npcElement = createNpc("Teleport");
        //spawnNpc(npcElement);

        //setPlayerInstance(npcElement, "WISP");
        //setPlayerPosition(npcElement, location.x, location.y, location.z);
    }

    function checkWereIn() {
        if(goesIn == true)
            return;

        local pos = getPlayerPosition(heroId);
        local dist = getDistance3d(pos.x, pos.y, pos.z, location.x, location.y, location.z);
        if(dist < 280)
        {
            goesIn = true;
            timerLeftOut = 5;
            setTimer(function(obj) {
                obj.goesIn = false;

                local pos = getPlayerPosition(heroId);
                local dist = getDistance3d(pos.x, pos.y, pos.z, obj.location.x, obj.location.y, obj.location.z);
                if(dist > 300)
                    return;

                local packet = Packet();
                packet.writeUInt8(Packets.Teleport);
                packet.writeUInt8(TeleportPackets.Use);
                packet.writeInt16(obj.id);
                packet.send(RELIABLE_ORDERED);
            }, 5000, 1, this);
            setTimer(function(obj) {
                local pos = getPlayerPosition(heroId);
                local dist = getDistance3d(pos.x, pos.y, pos.z, obj.location.x, obj.location.y, obj.location.z);
                if(dist > 300)
                    return;

                Chat.print("Teleportacja ("+timerLeftOut+")", -1);
                timerLeftOut = timerLeftOut - 1;
            }, 1000, 5, this);
        }
    }

    static function onRender()
    {
        if(check > getTickCount())
            return;

        foreach(element in All)
        {
            element.checkWereIn();

            if(getPlayerPosition(element.npcElement) != null)
                addEffect(element.npcElement, "spellFX_Teleport_RING");
        }

        check = getTickCount() + 1000;
    }
}


function getTeleport(id) {
    return All[id];
}

function addTeleport(locx,locy,locz,destx,desty,destz) {
    local model = Teleport();
    model.location = { x = locx, y = locy, z = locz}
    model.teleport = { x = destx, y = desty, z = destz}

    model.setUpMob();
}
