
addEventHandler("onPlayerDead", function(pid) {
    if(pid != heroId)
        return;
    
    foreach(itemId in PlayerItem.equpiedItems) {
	    PlayerItem.unEquipItemTable(itemId);
    }
})