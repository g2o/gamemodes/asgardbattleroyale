# Introduction

**Asgard Battle Royale** is a gamemode, that offers battle royale like gameplay style.  
The gamemode was created by the **Quachodron** and offers:
1. Complex monster AI
2. The narrowing circle of the game area
3. Game rounds
4. Clans support
5. UI for private messaging
6. Building
7. Mounts

## Gameplay

https://youtu.be/fiwXc8C_gOo

# Installation

1. Download the [addon](https://mega.nz/file/Y58xTLiC#3wMjnO3UEBPcA1lMXnxdyDRIPfxj8QrNab8klXCNqcA) and place it in main gamemode directory.
2. Include `scripts.xml` to your project, this can be done by adding this line to your `config.xml` file.  

Example:

```xml
<import src="gamemodes/asgardbattleroyale/scripts.xml" />
```

